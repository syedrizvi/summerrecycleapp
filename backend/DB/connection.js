const mysql = require('mysql');
const nonApiError = require('../errorhandler/nonApiError');
// const host = 'jdbc:mysql://localhost:3306/Peoples?autoReconnect=true&useSSL=false';//suppose to suppress ssl error in production
// const host = process.env.DB_HOST || "localhost";
const host = process.env.DB_HOST;

const db = mysql.createPool({
    host        : host,
    user        : process.env.DB_UNAME,
    password    : process.env.DB_PASSWRD,
    database    : process.env.DB_NAME
});

try{
    db.connect((error, next)=>{
        if(error){
            // throw error;
            // new nonApiError(`database connection failed\n${error}`);
            console.error(`database connection failed!!\n${"error number: " + error.errno}`);
            // console.log(error);
            if(error.errno === -4078){
                console.log("Database down");
            }
            if(error.errno === 1049){
                console.log("Problem with database name");
            }
            if(error.errno === 1045){
                console.log("Problems with user");
            }
            if (error.code === 'PROTOCOL_CONNECTION_LOST'){
                console.log(error.errno);
                console.log("PROTOCOL_CONNECTION_LOST");
                console.log("Connection was dropped, reconnecting!");
                setTimeout(createConnectionToSQL, 2000);
            }
            else {
                throw error;
            }
            //DEBT: Create error handler
            // next(ApiError.badRequest('connection failed: DB not running'));//we have to also create route for non express errors
            // return;
        }else{
            console.log("connected");
        }
    });
    
} catch(error){
    console.log("unknown db error occured!!!");
}

// let db_config = {
//     host        : host,
//     user        : process.env.DB_UNAME,
//     password    : process.env.DB_PASSWRD,
//     database    : process.env.DB_NAME
// };

// let db;

// function handleDisconnect() {
//     db = mysql.createConnection(db_config); // Recreate the connection, since
//                                             // the old one cannot be reused.

//     db.connect(function(err) {                  // The server is either down
//         if(err) {                                       // or restarting (takes a while sometimes).
//         console.log('error when connecting to db:', err);
//         setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
//         }                                   // to avoid a hot loop, and to allow our node script to
//     });                                     // process asynchronous requests in the meantime.
//     // If you're also serving http, display a 503 error.
//     db.on('error', function(err) {
//         console.log('db error', err);
//         if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
//             handleDisconnect();                         // lost due to either server restart, or a
//         } else {                                      // connnection idle timeout (the wait_timeout
//         throw err;                                  // server variable configures this)
//         }
//     });
// }

// handleDisconnect();

// module.exports = db;`

// const createConnectionToSQL = () => {
//     try{
//         connection = mysql.createConnection(db);
//         connection.connect = (error) => {
//             if(error){
//                 setTimeout(createConnectionToSQL, 2000);
//             }
//             else {
//                 console.log('connected');
//             }
//         }
//         connection.on('error', (error) => {
//             if(error){
//                 // throw error;
//                 // new nonApiError(`database connection failed\n${error}`);
//                 console.error(`database connection failed!!\n${"error number: " + error.errno}`);
//                 // console.log(error);
//                 if(error.errno === -4078){
//                     console.log("Database down");
//                 }
//                 if(error.errno === 1049){
//                     console.log("Problem with database name");
//                 }
//                 if(error.errno === 1045){
//                     console.log("Problems with user");
//                 }
//                 if (error.code === 'PROTOCOL_CONNECTION_LOST'){
//                     console.log(error.errno);
//                     console.log("PROTOCOL_CONNECTION_LOST");
//                     console.log("Connection was dropped, reconnecting!");
//                     setTimeout(createConnectionToSQL, 2000);
//                 }
//                 else {
//                     throw error;
//                 }
//                 //DEBT: Create error handler
//                 // next(ApiError.badRequest('connection failed: DB not running'));//we have to also create route for non express errors
//                 // return;
//             } else {
//                 console.log(connected);
//             }
//         });
        
//     } catch(error){
//         console.log("unknown db error occured!!!");
//     }
// }
// const db = mysql.createConnection({
//     host        : host,
//     user        : process.env.DB_UNAME,
//     password    : process.env.DB_PASSWRD,
//     database    : process.env.DB_NAME
// });

// const db = ({
//     host        : host,
//     user        : process.env.DB_UNAME,
//     password    : process.env.DB_PASSWRD,
//     database    : process.env.DB_NAME
// });

// connectToDB = function(){
//     connection = mysql.createConnection(db);
//     connection.connect(function (err) {
//         if(err){
//             setTimeout(connectToDB, 2000);
//         }
//         else {
//             console.log("connected");
//         }
//     });
//     connection.on('error', function(err) {
//         systemMessage('Error:' + err);
//         if(err.code === 'PROTOCOL_CONNECTION_LOST'){
//             connectToDB();
//         } else {
//             throw err;
//         }
//     });
// };

// systemMessage = function(message) {
//     //display error message
//     console.log('==============================================');
//     console.log(message);
//     console.log('==============================================');

// }
// createConnectionToSQL()
// module.exports = db;