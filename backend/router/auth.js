const router = require('express').Router();
const jwt = require("jsonwebtoken");
const {registerValidation, loginValidation, mysql_real_escape_string} = require('../validation');
const db = require('../DB/connection');
const bcrypt = require('bcryptjs');
const ApiError = require('../errorhandler/ApiError');


/*****************Routes *********** */

//Some future Features that could make this page stronger: store this in database or redis case and delete the refresh token from database when user logs out. 
// Or if you have access token stolen then it will be deleted once logged out. 
//Although all tokens generated have expiration of 15 minutes. After which tokens will no longer be useful.
// We could push to this variable array the access tokens.
let refreshTokens = [];

router.post('/register', async (req,res, next) => {
    const {error} = registerValidation(req.body);
    if (error) return res.send({error: error.details[0].message});

    const email = mysql_real_escape_string(req.body.email);
    // email = email.toLowerCase();
    
    try{
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt);

        let sql = "INSERT INTO USERS(email, password, user_role) VALUES ?";
        values = [
            [email, hashedPassword,'customer']
        ];

        /*******enter database insert here */
        db.query(sql, [values], (error, result) => {
            if (error) return res.send({message: "Invalid Credentials"});
            res.send({message: "success!!", id: result.insertId});
        });
        /*************end database******** */
    } catch(err){
        next(ApiError.badRequest(err.message));
    }
});

router.post('/registerUserInfo', async (req,res, next) => {
    const {error} = registerValidation(req.body);
    if (error) return res.send({error: error.details[0].message});

    const email = mysql_real_escape_string(req.body.email);
    // email = email.toLowerCase();
    const firstName = mysql_real_escape_string(req.body.firstname);
    const lastName = mysql_real_escape_string(req.body.lastname);
    const streetAddress = mysql_real_escape_string(req.body.street_addr);
    const zip = mysql_real_escape_string(req.body.zip);
    const city = mysql_real_escape_string(req.body.city);
    const state = mysql_real_escape_string(req.body.state);
    const phone = mysql_real_escape_string(req.body.phone);
    
    try{
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt);

        let sql = "INSERT INTO USERS(email, password, firstname, lastname, street_addr, zip, city, state, phone, user_role) VALUES ?";
        values = [
            [email, hashedPassword, firstName, lastName, streetAddress, zip, city, state, phone, 'customer']
        ];

        /*******enter database insert here */
        db.query(sql, [values], (error, result) => {
            if (error) 
            {
                //res.send({message: "user already exists"});
                let sqlUserId = "SELECT user_id FROM users WHERE email = ?";
                let value = [email];
                db.query(sqlUserId, [value], (err, result) => {
                    if(err)
                    {
                        res.send({});
                    }
                    else {
                        res.send({message: "user already exists", id: result[0].user_id});
                    }
                })
            }
            else 
            {
                res.send({message: "success!!", id: result.insertId});
            }
            
        });
        /*************end database******** */
    } catch(err){
        next(ApiError.badRequest(err.message));
    }
});

const generateAccessToken = (id, email, userrole)=>{
    return jwt.sign({ id:id, email: email, userrole: userrole },
        process.env.TOKEN_ASECRET,
        { expiresIn: "2m"});
}

const generateRefreshToken = (id, email, userrole)=>{
    return jwt.sign({ id:id, email: email, userrole: userrole },
        process.env.TOKEN_RSECRET,
        { expiresIn: "15m"});
}
// comment this route if we don't want general signin.
router.post("/login", (req, res, next) => {
    const {error} = loginValidation(req.body);
    if (error) return res.status(400).send({"message1": error.details[0].message});

    
    try{
        const email = mysql_real_escape_string(req.body.email);
        // email = email.toLowerCase();
        const password = mysql_real_escape_string(req.body.password);
        // let sql = "Select user_id, email, password From users WHERE email = ? && user_role = 'customer'";//uncomment this when Driver, customer and admin have their own pages.
        let sql = "Select user_id, email, password, user_role From users WHERE email = ?";//comment this when driver, customer, admin have their own pages. if we don't care what user is signing in
        db.query(sql, email, async (err, result) => {
            if(err){
                // res.status(400).send({"message": err});
                next(ApiError.badRequest("Email or password not found"));
                return;
            }
            if(result.length > 0){
                const validPass = await bcrypt.compare(password, result[0].password);
                if(!validPass) return res.send({message: "Email or password not found"});
                const userid = result[0].user_id,
                      useremail = result[0].email,
                      user_role = result[0].user_role;
                const token = jwt.sign({_id: userid, email: useremail}, process.env.TOKEN_ASECRET);
                
                const accessToken = generateAccessToken(userid, useremail, user_role);
                const refreshToken = generateRefreshToken(userid, useremail, user_role);//you dont have to make new refresh token but it is safer.
                refreshTokens.push(refreshToken);//Debt: this is where we would push in the database or redis cache if we want faster response
                
                let sql2 = "INSERT INTO authtoken(user_id, refreshtoken) VALUES ?";
                let values2 = [
                    [userid, refreshToken]
                ];
                db.query(sql2, [values2], (error, result) =>{
                    
                });

                // res.setHeader({auth-token: "Bearer" + accessToken});
                res.json({
                    id: result[0].user_id, 
                    email: result[0].email,
                    accessToken,
                    refreshToken
                });
            } else {
                res.send({message: "Email or password not found"});
            }
        });
    } catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest('Email or password not found'));
    }
});
//uncomment this route if we care about what userrole is signing in and comment the one above it that one is general signin. This one is for customer only.
// router.post("/login", (req, res, next) => {
//     const {error} = loginValidation(req.body);
//     if (error) return res.status(400).send({"message1": error.details[0].message});

    
//     try{
//         const email = req.body.email;
//         const password = req.body.password;
//         let sql = "Select user_id, email, password From users WHERE email = ? && user_role = 'customer'";//uncomment this when Driver, customer and admin have their own pages.
//         // let sql = "Select user_id, email, password From users WHERE email = ?";//comment this when driver, customer, admin have their own pages. if we don't care what user is signing in
//         db.query(sql, email, async (err, result) => {
//             if(err){
//                 // res.status(400).send({"message": err});
//                 next(ApiError.badRequest("Email or password not found"));
//                 return;
//             }
//             if(result.length > 0){
//                 const validPass = await bcrypt.compare(password, result[0].password);
//                 if(!validPass) return res.send({message: "Email or password not found"});
//                 const userid = result[0].user_id,
//                       useremail = result[0].email;
//                 const token = jwt.sign({_id: userid, email: useremail}, process.env.TOKEN_ASECRET);
                
//                 const accessToken = generateAccessToken(userid, useremail, "customer");
//                 const refreshToken = generateRefreshToken(userid, useremail, "customer");//you dont have to make new refresh token but it is safer.
//                 refreshTokens.push(refreshToken);//Debt: this is where we would push in the database or redis cache if we want faster response
                
//                 let sql2 = "INSERT INTO authtoken(user_id, refreshtoken) VALUES ?";
//                 let values2 = [
//                     [userid, refreshToken]
//                 ];
//                 db.query(sql2, [values2], (error, result) =>{
                    
//                 });

//                 // res.setHeader({auth-token: "Bearer" + accessToken});
//                 res.json({
//                     id: result[0].user_id, 
//                     email: result[0].email,
//                     accessToken,
//                     refreshToken
//                 });
//             } else {
//                 res.send({message: "Email or password not found"});
//             }
//         });
//     } catch(err){
//         // res.status(400).send(err);
//         next(ApiError.badRequest('Email or password not found'));
//     }
// });

router.post("/refresh", (req, res,next)=>{
    // take the refresh token from the use\
    const refreshToken = req.body.token;
    //send error if there is no token or it's invalid
    if(!refreshToken) return res.status(401).json("You are not authenticated!");
    //if refresh tokens array doesn't include this token then send another. For future if passwords need to be stored in database
    // if(!refreshTokens.includes(refreshToken)){
    //     return res.status(403).json("Refresh token is not valid!");
    // }
    jwt.verify(refreshToken, process.env.TOKEN_RSECRET, (err, user)=> {
        // err && console.log(err);
        // console.log(user)
        if(err){
            next(ApiError.badRequest("Session Expired"));
            // return;
        }else{

            // refreshTokens = refreshTokens.filter((token) => token !== refreshToken);
            
            const newAccessToken = generateAccessToken(user.id, user.email, user.userrole);
            const newRefreshToken = generateRefreshToken(user.id, user.email, user.userrole);
    
            // refreshTokens.push(newRefreshToken);//replace it with database
    
            res.status(200).json({
                accessToken: newAccessToken,
                refreshToken: newRefreshToken,
            });
        }
    });
    //if everything is ok, create new access token, refresh  token and send to user
});

//verify tokens
const verify = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if(authHeader){
        const token = authHeader.split(" ")[1];//get token from header and remove bearer to verify

        jwt.verify(token, process.env.TOKEN_ASECRET,(err, user)=>{
            if(err){
                // return res.status(401).json("Token is not valid!");//token failed too long access denied
                next(ApiError.badRequest401('Token is not valid!'));
                return;
            }
            req.user = user;
            next();
        });
        
    }else{
        next(ApiError.badRequest401('You are not authenticated'));
    }
};

// logout will check for refreshToken in body and header will require acessToken
router.post("/logout", (req, res) =>{
    // const refreshToken = req.body.token;
    // refreshTokens = refreshTokens.filter((token) => token !== refreshToken);//if the refresh tokens are being store in database. They need to be cleared.
    res.status(200).json("You logged out successfully.");
});

module.exports = router;
