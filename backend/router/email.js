const router = require('express').Router();
const nodemailer = require('nodemailer');
const {mysql_real_escape_string} = require('../validation');

// router.post('/email', (req, res) => {
//     console.log("hello"); 
// });

  // send mail with defined transport object
//   let info = await transporter.sendMail({
//     from: '"Recyclones Support" <dev@recyclones.com>', // sender address
//     to: "managerrecyclones@gmail.com", // list of receivers
//     subject: "Email Verification", // Subject line
//     text: "Hello world?", // plain text body
//     html: "<b>Hello world?</b>", // html body
//   });

//   console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
//   console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  router.post('/newuser', async (req,res) => {    
    const email = mysql_real_escape_string(req.body.email);
    // send mail with defined transport object
    try{
      let info = await transporter.sendMail({
        from: '"Recyclones Support" <dev@recyclones.com>', // sender address
        to: email, // list of receivers
        subject: "Email Verification", // Subject line
        text: "Hello world?", // plain text body
        html: "<b>Hello world?</b>", // html body
        });
        transporter.sendMail(info, (error, info) => {
          if(error) {
              return error;
          }
          res.send({message: "Mail send", message_id: info.messageId});
      })
    } catch (err) {
      res.send(error.message);
    }
});


router.post('/testing', (req, res) => {
  const { to, cc, bcc, subject, message, attachment, smtpDetails } = req.body;

  // if (!to || !subject || !message || !smtpDetails) return res.status(400).send('input cannot be empty')

  // let transporter = nodemailer.createTransport({
  //     service: 'gmail',
  //     auth: {
  //         user: '...@gmail.com',
  //         pass: '...'
  //     }
  // });
  //transport configuration for user a site server to send an email.
  let transporter = nodemailer.createTransport({
    // This is the SMTP mail server to use for notifications. 
    // GCDS uses this mail server as a relay host.
    host: "smtp.gmail.com",
    // SMTP is unlike most network protocols, which only have a single port number. 
    // SMTP has at least 3. They are port numbers 25, 587, and 465.
    // Port 25 is still widely used as a **relay** port from one server to another.
    // Port for SSL: 465
    // Port for TLS/STARTTLS: 587
    port: 465,
    //  if true the connection will use TLS when connecting to server. If false (the 
    // default) then TLS is used if server supports the STARTTLS extension. In most 
    // cases set this value to true if you are connecting to port 465. For port 587 or 
    // 25 keep it false
    secure: true, // use TLS
    auth: {
        // Your full email address
        user: process.env.SMTP_TO_EMAIL,
        // Your Gmail password or App Password
        pass: process.env.SMTP_TO_PASSWORD
    }
  });

  let mailOptions = {
      from:  'managerrecyclones@gmail.com',
      to: to,
      cc: cc,
      bcc: bcc,
      subject: subject,
      text: `${message}`
  };

  transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
          console.log(error);
          res.send('mail not sent')

      } else {
          console.log('Email sent: ' + info.response);
          res.send('mail sent')
      }
  });
});
  

module.exports = router;