const router = require('express').Router();
const verify = require('./verifyToken');

router.get('/posts',  verify, (req, res) => {
    res.send( "Accessed this route with the token");
})

module.exports = router;