const jwt = require('jsonwebtoken');
// const ApiError = require('../errorhandler/nonApiError');

module.exports = function (req, res, next){
    // const token = req.header('auth-token');
    // console.log(req);
    let token = req.headers.authorization;
    if(token){
        token = token.split(" ")[1];
        try{
            const verified = jwt.verify(token, process.env.TOKEN_ASECRET);
            next();
        } catch (err) {
            res.status(400).send('Invalid Token');
        }
    }else {
        return res.status(401).send('Access Denied');
    }
};