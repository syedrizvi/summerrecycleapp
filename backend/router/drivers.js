const db = require('../DB/connection');
const router = require('express').Router();
const {changePasswordValidation, mysql_real_escape_string} = require('../validation');
const ApiError = require('../errorhandler/ApiError');
const verify = require('./verifyToken');


// Get user info route
router.post('/getUserInfo', verify, (req, res, next) => {
    const userId = mysql_real_escape_string(req.body.userId);

    try{
        let sql = "SELECT user_id, email, phone, firstname, lastname, street_addr, city, state, zip, user_role FROM users WHERE user_id = ?";

        db.query(sql, [userId], (err, result) => {
            if(err){
                return res.send("Request failed. Invalid request!");
            }
            else if(result.length > 0){
                console.log(result);
                res.send(result[0]);
            } else {
                res.send(result[0])
                // next(ApiError.badRequest("Request failed. Invalid request!"));
            }
        });
    } catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest("Request failed. Invalid request!"));
    }
});


// Get order info route
router.post('/getOrderInfo', verify, (req, res, next) => {
    try {
        // Get request body
        let driverId = mysql_real_escape_string(req.body.driverId);

        // SQL SELECT all orders using user_id
        let sqlGetOrders = "SELECT * FROM orders WHERE driver_id = ?";
        // SQL query 
        db.query(sqlGetOrders, [driverId], ((err, result) => {
            // error
            if(err) {
                next(ApiError.badRequest(err));
            }
            // no user found
            else if (result.length == 0) {
                res.header("Access-Control-Allow-Origin", "*");
                res.header("Access-Control-Allow-Credentials", "true");
                res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
                res.send("user not found");
                // console.log(result);
            }
            // send order info
            else {
                res.header("Access-Control-Allow-Origin", "*");
                res.header("Access-Control-Allow-Credentials", "true");
                res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
                res.send(result);
                // console.log(result);
            }
        }));
    }
    catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest('invalid request'));
    }
});

// Get box info route
router.post('/getBoxInfo', verify, (req, res, next) => {
    let orderId = mysql_real_escape_string(req.body.orderId);
    
    try{
        let sql = "SELECT * FROM order_boxdetail WHERE order_id = ?";

        db.query(sql, [orderId], (err, result) => {
            if(err){
                return res.send("Request failed. Invalid request!");
            }
            else if(result.length > 0){
                console.log(result);
                res.send(result[0]);
            } else {
                res.send(result[0]);
                // next(ApiError.badRequest("Request failed. Invalid request!"));
            }
        });
    } catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest("Request failed. Invalid request!"));
    }
});

router.post('/updateorderstatus', verify, (req, res, next) =>{
    res.send("successful at updating");
});

router.post("/orders/updatestatus", verify, (req, res, next) => {
    const order_id = mysql_real_escape_string (req.body.orderId);
    const driver_id = mysql_real_escape_string (req.body.driverId);
    const order_status = mysql_real_escape_string (req.body.order_status);

    try{
        let sql = `UPDATE ORDERS SET order_status = \'${order_status}\' WHERE order_id = \'${order_id}\'`;

        db.query(sql, async (err, result) => {
            if(err){
                return res.send(err.message);                 
                // res.send(result);
                // res.status(400).send({"message": err});
                // next(ApiError.badRequest("Invalid Request"));
                // res.send(err);
                // return
            }
            else if(result.length > 0){
                console.log(result);
                res.send("Order status updated to " + order_status);
                
            } else {
                res.send("Order status updated to " + order_status);
                // next(ApiError.badRequest('Email or password not found'));
            }
        });
    } catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest(err));
    }
});

module.exports = router;