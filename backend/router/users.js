const db = require('../DB/connection');
const router = require('express').Router();
const {changePasswordValidation, mysql_real_escape_string} = require('../validation');
const ApiError = require('../errorhandler/ApiError');
const verify = require('./verifyToken');

// bcrypt information
const bcrypt = require('bcryptjs');
const saltRounds = 10;
let newHashPassword = null;

// Input values
let testUserId = "1";
let testOldPassword = "test222"; 
let testNewPassword = "test333"; 


// Get user info route
router.post('/getUserInfo', verify, (req, res, next) => {
    const userId = mysql_real_escape_string(req.body.userId);

    try{
        let sql = "SELECT user_id, email, phone, firstname, lastname, street_addr, city, state, zip, user_role FROM users WHERE user_id = ?";

        db.query(sql, [userId], (err, result) => {
            if(err){
                return res.send("Request failed. Invalid request!");
            }
            else if(result.length > 0){
                // console.log(result);
                res.send(result[0]);
            } else {
                res.send(result[0]);
                // next(ApiError.badRequest("Request failed. Invalid request!"));
            }
        });
    } catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest("Request failed. Invalid request!"));
    }
});

// Get order info route
/*router.post('/getOrderInfo', (req, res, next) => {
    try {
        // Get request body
        let {userId} = req.body.userId;
        let tryUserId = mysql_real_escape_string(userId);

        // SQL SELECT all orders using user_id
        let sqlGetOrders = "SELECT * FROM orders WHERE user_id = ?";
        let sqlInjectData = [tryUserId];
        // SQL query 
        db.query(sqlGetOrders, sqlInjectData, ((err, result) => {
            // error
            if(err) {
                throw err;
            }
            // no user found
            else if (result.length == 0) {
                res.header("Access-Control-Allow-Origin", "*");
                res.header("Access-Control-Allow-Credentials", "true");
                res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
                res.send("user not found");
                console.log(result);
            }
            // send order info
            else {
                res.header("Access-Control-Allow-Origin", "*");
                res.header("Access-Control-Allow-Credentials", "true");
                res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
                res.send(result);
                console.log(result);
            }
        }));
    }
    catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest('invalid request'));
    }
});*/


// Get order info route
router.post('/getOrderInfo', verify, (req, res, next) => {
    try {
        // Get request body
        const userId = mysql_real_escape_string(req.body.userId);

        // SQL SELECT all orders using user_id
        const sqlGetOrders = "SELECT * FROM orders WHERE user_id = ?";
        const sqlInjectData = [userId];
        // SQL query 
        db.query(sqlGetOrders, sqlInjectData, (err, result) => {
            // error
            if(err) {
                throw err;
            }
            // send order info
            else if (result.length > 0) {
                res.header("Access-Control-Allow-Origin", "*");
                res.header("Access-Control-Allow-Credentials", "true");
                res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
                res.send(result);
                // console.log(result);
            }
            // no user found 
            else {
                res.header("Access-Control-Allow-Origin", "*");
                res.header("Access-Control-Allow-Credentials", "true");
                res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
                res.send("user not found");
                // console.log("userId: " + req.body.userId);
            }
        });
    }
    catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest('invalid request'));
    }
});


// Get box info route
router.post('/getBoxInfo', verify, (req, res, next) => {
    let orderId = mysql_real_escape_string(req.body.orderId);
    //orderId = 1;
    console.log(orderId)
    try{
        let sql = "SELECT * FROM order_boxdetail WHERE order_id = ?";

        db.query(sql, [orderId], (err, result) => {
            if(err){
                return res.send("Request failed. Invalid request!");
            }
            else if(result.length > 0){
                // console.log(result);
                res.send(result[0]);
            } else {
                res.send(result[0]);
                // next(ApiError.badRequest("Request failed. Invalid request!"));
            }
        });
    } catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest("Request failed. Invalid request!"));
    }
});

// Change password route
router.post('/changePassword', verify, (req, res, next) => { 
    try {
        // Get request body
        let {userId, oldPassword, newPassword} = req.body;
        let testOldPassword = mysql_real_escape_string(oldPassword);
        let testNewPassword = mysql_real_escape_string(newPassword);

        // console.log(oldPassword);
        // console.log(newPassword);
        // SQL SELECT current password using user_id
        let sqlFindPassword = "SELECT password FROM users WHERE user_id = ?";
        let data = [userId];
        // SQL query 
        db.query(sqlFindPassword, data, ((err, result) => {
            // error
            if(err) {
                res.send(err);
                throw err;
            }
            // no user found
            else if (result.length == 0) {
                res.send("user not found");
            }
            // update new password
            else {
                // console.log(result);
                // old password hashed from db
                let hashPassword = result[0].password;
                // test for match between old password input
                bcrypt.compare(testOldPassword, hashPassword, ((err, response) => {
                    if (err) {
                        res.status(200).json(err);
                        res.send(err);
                    }
                    else if (response != true) {
                        res.header("Access-Control-Allow-Origin", "*");
                        res.header("Access-Control-Allow-Credentials", "true");
                        res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                        res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
                        res.send("password not found");
                    }
                    else {
                        // console.log("password matched");
                        // hash new password
                        bcrypt.hash(testNewPassword, saltRounds, (err, hash) => {
                            if(err) {
                                //res.send(err);
                                next(ApiError.badRequest('invalid request'));
                            }
                            newHashPassword = hash;
                            // console.log(newHashPassword);

                            // SQL UPDATE password with new password
                            let userId = testUserId;
                            let sqlChangePassword = "UPDATE users SET password = ? WHERE user_id = ?;";
                            let data = [hash, userId];
                            db.query(sqlChangePassword, data, ((err, result) => {
                                if (err) {
                                    next(ApiError.badRequest('invalid request'));
                                }
                                else {
                                    res.header("Access-Control-Allow-Origin", "*");
                                    res.header("Access-Control-Allow-Credentials", "true");
                                    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                                    res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
                                    res.send("password changed");
                                }
                            }))
                        });
                    }
                })); 
            }
        }));
    }
    catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest('invalid request'));
    }
});

// Change info route
router.post('/changeInfo', verify, (req, res, next) => { 
    try {
        // Get request body
        let {userId, userInfoColumn, newInfo} = req.body;
        let tryUserInfoColumn = mysql_real_escape_string(userInfoColumn);
        let tryNewInfo = mysql_real_escape_string(newInfo);

        // SQL code for finding user
        let sqlFindUser = "SELECT user_id FROM users WHERE user_id = ?";
        let sqlInjectData = userId;
        // SQL query 
        db.query(sqlFindUser, sqlInjectData, ((err, result) => {
            // error
            if(err) {
                res.send(err);
                throw err;
            }
            // no user found
            else if (result.length == 0) {
                res.send("user not found");
            }
            // update new password
            else {
                // SQL code for changing first name
                let sqlChangeInfo = "UPDATE users SET " + tryUserInfoColumn + " = ? WHERE user_id = ?;";
                let sqlInjectData = [tryNewInfo, userId];
                // SQL query
                db.query(sqlChangeInfo, sqlInjectData, ((err, result) => {
                    if (err) {
                        next(ApiError.badRequest('invalid request'));
                    }
                    else {
                        res.header("Access-Control-Allow-Origin", "*");
                        res.header("Access-Control-Allow-Credentials", "true");
                        res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                        res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
                        res.send("user info changed");
                    }
                }))
            }
        }))
    }
    catch(err){
        next(ApiError.badRequest('invalid request'));
    }
});

// Change date and time route
router.post('/changeDateTime', verify, (req, res, next) => { 
    try {
        // Get request body
        let tryOrderId = mysql_real_escape_string(req.body.orderId);
        let tryNewDate = mysql_real_escape_string(req.body.date);
        let tryNewTime = mysql_real_escape_string(req.body.time);
        console.log(tryNewDate);
        console.log(tryNewTime);

        // SQL code for finding user
        let sqlFindOrder = "SELECT order_date, order_time FROM orders WHERE order_id = ?";
        // SQL query
        db.query(sqlFindOrder, [tryOrderId], ((err, result) => {
            if(err) {
                // error
                res.send(err);
                throw err;
            }
            else if (result.length > 0) {
                // update new password
                // SQL code for changing first name
                let sqlChangeDateTime = "UPDATE orders SET order_date = ?, order_time = ? WHERE order_id = ?;";
                let sqlInjectData = [tryNewDate, tryNewTime, tryOrderId];
                // SQL query
                db.query(sqlChangeDateTime, sqlInjectData, ((error, result) => {
                    if (error) {
                        next(ApiError.badRequest('invalid request'));
                    }
                    else {
                        res.send("date and time changed");
                    }
                }))  
            }
            else {
                // no user found
                res.send("order not found");
            }
        }))
    }
    catch(err){
        next(ApiError.badRequest('invalid request'));
    }
});
// Change order status route
router.post('/changeOrderStatus', verify, (req, res, next) => { 
    try {
        // Get request body
        let tryOrderId = mysql_real_escape_string(req.body.orderId);
        let tryOrderStatus = mysql_real_escape_string(req.body.orderStatus);

        // SQL code for finding user
        let sqlFindOrder = "SELECT order_status FROM orders WHERE order_id = ?";
        // SQL query
        db.query(sqlFindOrder, [tryOrderId], ((err, result) => {
            if(err) {
                // error
                res.send(err);
                throw err;
            }
            else if (result.length > 0) {
                // update new password
                // SQL code for changing first name
                let sqlChangeOrderStatus = "UPDATE orders SET order_status = ? WHERE order_id = ?;";
                let sqlInjectData = [tryOrderStatus, tryOrderId];
                // SQL query
                db.query(sqlChangeOrderStatus, sqlInjectData, ((error, result) => {
                    if (error) {
                        next(ApiError.badRequest('invalid request'));
                    }
                    else {
                        res.send("order status changed");
                    }
                }))  
            }
            else {
                // no user found
                res.send("order not found");
            }
        }))
    }
    catch(err){
        next(ApiError.badRequest('invalid request'));
    }
});



module.exports = router;