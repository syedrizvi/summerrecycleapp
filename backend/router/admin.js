const router = require('express').Router();
const {mysql_real_escape_string} = require('../validation');
const db = require('../DB/connection');
const ApiError = require('../errorhandler/ApiError');
const verify = require('./verifyToken');


router.get('/orders/unassigned', verify, (req, res, next) =>{
    try{
        // let sql = "SELECT * From orders WHERE driver_id IS NULL";
        // let sql = "SELECT order_id, orders.user_id, driver_id, order_date, order_time, total, order_status, users.street_addr, users.city FROM orders, users WHERE orders.user_id=users.user_id AND orders.driver_id is NULL";
        let sql = "SELECT order_id, orders.user_id, driver_id, order_date, order_time, total, order_status, users.street_addr, users.city, users.firstname FROM orders, users WHERE orders.user_id=users.user_id AND orders.driver_id is NULL";
        // SELECT order.order_id, order.user_id, order.driver_id, order.order_date, order.order_time, order.total, order.order_status, users.steet_addr, users.city
        // FROM orders
        // INNER JOIN users
        // ON orders.user_id=users.user_id;


        db.query(sql, (err, result) => {
            if(err){
                return res.send("Request failed. Invalid request!");
            }
            else if(result.length > 0){
                // console.log(result);
                res.send(result);
            } else {
                res.send(result)
                // next(ApiError.badRequest("Request failed. Invalid request!"));
            }
        });
    } catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest("Request failed. Invalid request!"));
    }
});

router.get('/drivers/available/id', verify, (req, res, next) =>{
    try{
        let sql = "SELECT user_id FROM users WHERE user_role = 'driver'";
        
        db.query(sql, (err, result) => {
            if(err){
                return res.send("Request failed. Invalid request!");
            }
            else if(result.length > 0){
                // console.log(result);
                res.send(result);
            } else {
                res.send(result)
            }
        });
    } catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest("Request failed. Invalid request!"));
    }
});

router.post('/orders/assignDrivers', verify, (req, res, next) =>{
    const orderId = mysql_real_escape_string(req.body.orderId);
    const driverId = mysql_real_escape_string(req.body.driverId);
    try{
        let sql = `UPDATE orders SET driver_id=${driverId} WHERE order_id = \'${orderId}\'`;
        
        db.query(sql, (err, result) => {
            if(err){
                return res.send("Request failed. Invalid request!");
            }
            else if(result.length > 0){
                // console.log(result);
                next(ApiError.badRequest401("Request failed. Invalid request!"));
            } else {
                return res.send(result)
            }
        });
    } catch(err){
        // res.status(400).send(err);
        next(ApiError.badRequest("Request failed. Invalid request!"));
    }
    // return res.send("Drivers Assigned");
})




module.exports = router;
