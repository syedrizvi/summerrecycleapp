const db = require('../DB/connection');

const router = require('express').Router();

// bcrypt information
const bcrypt = require('bcrypt');
const saltRounds = 10;
let newHashPassword = null;

// Input values
let testUserId = "1";
let testOldPassword = "test222";
let testNewPassword = "test333";



// Change password route
router.post('/users/changePassword', (req, res) => {

    //let testOldPassword = req.body.oldPassword;
    //let testNewPassword = req.body.newPassword;
    let {oldPassword, newPassword} = req.body;
    let testOldPassword = oldPassword;
    let testNewPassword = newPassword;
    console.log(oldPassword);
    console.log(newPassword);

    try {
        let sqlFindPassword = "SELECT password FROM users WHERE user_id = ?";
    let data = [testUserId];
    // SQL query 
    db.query(sqlFindPassword, data, ((err, result) => {
        // error
        if(err) {
            throw err;
        }
        // no user found
        else if (result.length == 0) {
            console.log(result);
            console.log("user not found");
        }
        // update new password
        else {
            console.log(result);
            // old password hashed from db
            let hashPassword = result[0].password;
            // test for match between old password input
            bcrypt.compare(testOldPassword, hashPassword, ((err, response) => {
                if (err) {
                    throw err;
                }
                else if (response != true) {
                    console.log("password not found");
                }
                else {
                    console.log("password matched");
                    // hash new password
                    bcrypt.hash(testNewPassword, saltRounds, (err, hash) => {
                        if(err) {
                            throw err;
                        }
                        newHashPassword = hash;
                        console.log(newHashPassword);

                        // SQL UPDATE password with new password
                        let userId = testUserId;
                        let sqlChangePassword = "UPDATE users SET password = ? WHERE user_id = ?;";
                        let data = [hash, userId];
                        db.query(sqlChangePassword, data, ((err, result) => {
                            // error
                            try {
                                console.log(result);
                                console.log("password changed");
                                res.send(result);
                            }
                            catch (err) {
                                console.log("password not changed");
                                return res.send(err);
                            }
                            // password change successful
                            
                        }))
                    });
                }
            })); 
        }
    }));
    }
    // SQL SELECT current password using user_id
    catch (err) {
        res.status(400).send(err.message);
    }
    
});



module.exports = router;