const router = require('express').Router();
const verify = require("./verifyToken");

require('dotenv').config();//for environment variables in dotenv file
const db = require('../DB/connection');

router.get('/query', verify, (req, res) => {
      let sql = "SELECT * FROM users";
        db.query(sql, (err, result) => {
        if(err) return res.send(err);
        res.send(result);
    });
});

/***********Tested Register with get: works!! ********** */
router.get("/register", (req, res) => {
    let email = "test3@gmail.com", 
    password = "test333",
    streetAddress = "333 test street apt 333",
    state = "CA", 
    zipCode = "94203";
    let sql = "INSERT INTO USERS(email, password, street_addr, state, zip, user_role) VALUES ?";
    values = [
        [email, password, streetAddress, state, zipCode, 'customer']
    ]
    db.query(sql, [values], ((err, result) => {
        if(err) return res.send(err);
        // console.log("user added!!");
        res.send(result);
    }));
    console.log('registered!');
});
/***********Test with post with body parser: works!!********** */
router.post("/registerbodyparser", (req, res) => {
    let {email, password, streetAddress, state, zipCode} = req.body;
    let sql = "INSERT INTO USERS(email, password, street_addr, state, zip, user_role) VALUES ?";
    values = [
        [email, password, streetAddress, state, zipCode, 'customer']
    ]
    db.query(sql, [values], ((err, result) => {
        // if(err) throw err; 
        if(err) return res.send(err);
        // console.log("user added!!");
        res.send(result + "**********************registered post!");
    }));
    console.log('registered post!');
});

// we could use hapi/joi to sanitize 

// router.get('/authenticate', (req, res) => {
    //for registeration
        // prepared statement
    // let sqlQuery = "INSERT INTO USERS(email, password, street_addr, state, zip, user_role) VALUES (@email, @password, @streetAddress, @state, @zipCode, 'customer' )";
    // let preparedStatement = new sqlQuery.preparedStatement();
    // preparedStatement.input("email", sqlVarChar(50));
    // preparedStatement.input("password", sqlVarChar(50));
    // preparedStatement.input("street_addr", sqlVarChar(150));
    // preparedStatement.input("state", sqlVarChar(2));
    // preparedStatement.input("zip", sqlVarChar(5));
    // preparedStatement.input("user_role", sqlVarChar(10));
    // preparedStatement.prepare(sqlQuery)
    // .then(() => {
    //     return preparedStatement.execute({"email": email, "password": password, "streetAddress": streetAddress, "state": state, "zip": zipCode, "user_role": "user_role"})
    // })

    //***********end for regiteration */


    //debt: create a global variable loggedIn
    // let loggedIn = false;
    // const username = req.query.username,
    //     password = req.query.password;
    // let preparedStatement = new sql.preparedStatement(),
    //     sqlQuery = "select * from users where (username = @username and password = @password";

    // preparedStatement.input("username", sqlVarChar(50))
    // preparedStatement.input('password', sqlVarChar(50))
    // preparedStatement.prepare(sqlQuery)
    // .then(() => {
    //     return preparedStatement.execute({username: username, password: password})
    //         .then((recordSet) =>{
    //             if(recordSet) {
    //                 loggedIn = true;
    //                 res.send("logged in");
    //                 // successful log in
    //             }else {
    //                 //authentication failed
    //             }
    //         })
    // })

// })

module.exports = router; 