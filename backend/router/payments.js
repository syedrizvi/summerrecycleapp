const router = require('express').Router();
const db = require('../DB/connection');
const verify = require('./verifyToken');
const ApiError = require('../errorhandler/ApiError');
const {mysql_real_escape_string} = require('../validation');

router.post("/createOrderEntry", (req, res) => {

    const userId = mysql_real_escape_string(req.body.orderInfo.userId)
    const orderDate = mysql_real_escape_string(req.body.orderInfo.orderDate)
    const orderTime = mysql_real_escape_string(req.body.orderInfo.orderTime)
    const notes = mysql_real_escape_string(req.body.orderInfo.notes)
    const total = mysql_real_escape_string(req.body.orderInfo.total)

    try{
        let sql = "INSERT INTO ORDERS (user_id, order_date, order_time, pick_up_placement_note, total, order_status) VALUES ?"
        values = [
            [userId, orderDate, orderTime, notes, total, 'not_delivered']
    ]
    db.query(sql, [values], (err, result) => {
        if(err) return res.send(err);
        else res.send("order placed")
    });
    } catch(err)
    {
        next(ApiError.badRequest(err.message));
    }
})

module.exports = router;