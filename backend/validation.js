// Documentation: https://joi.dev/api/?v=17.4.2
// Validation
const Joi = require('@hapi/joi');

//We could sanitize input using joi


//register validation
const registerValidation = (data) => {
    const schema = Joi.object({
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required(),
        passwordRepeat: Joi.ref('password')
    });
    return schema.validate(data);
}

// login validation
const loginValidation = (data) => {
    const schema = Joi.object({
        email: Joi.string().min(6).required().email(),
        password: Joi.string()
        .min(6)
        .required()
    });
    return schema.validate(data);

};

//passwordvalidation
const changePasswordValidation = (data) => {
    const schema = Joi.object({
        newPassword: Joi.string().min(6).required()
    });
    return schema.validate(data);
}

//we could also research more on sql string package: npm install sqlstring.

const mysql_real_escape_string = (str) => {
    if (typeof str != 'string')
        return str;

    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
}

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.mysql_real_escape_string = mysql_real_escape_string;
module.exports.changePasswordValidation = changePasswordValidation;