const express = require('express');
const cors = require('cors');
const mysql = require('mysql');
const jwt = require("jsonwebtoken");
const cookieParser = require('cookie-parser');
require('dotenv').config();//for environment variables in dotenv file

//
const authRoute = require('./router/auth');
const dbRoute = require('./router/testusers');
const adminRoute = require('./router/admin');
const emailRoute = require('./router/email');
const apiErrorHandler = require('./errorhandler/error.handler');
const postRoute = require('./router/posts');
const twillio = require('./router/twillio');
const paypal = require('./router/paypal');
const usersRoute = require('./router/users');
const driversRoute = require('./router/drivers');
const paymentRoute = require('./router/payments');

const app = express();
const port = process.env.PORT || 5000;//process environment variable

// middleware
app.use(express.json());//turn data in json
app.use(cors({
    origin: ["http://localhost:3000"],
    methods: ["GET", "POST:"],
    credentials: true
}));
app.use(cookieParser());
// app.use(session({
//     key: "userId",
//     secret: process.env.SESSION_SECRET,
//     resave: false,
//     saveUninitialized: false
// }))
/************database*********/
//test api to check if database is working: localhost:5000/another/api/query
app.use('/another/api', dbRoute);
/****************End database*********/

/********** JWT implementation ********/
app.use('/api', authRoute);
app.use('/api', postRoute);

/***************JWT end*******/
app.use('/api/users', usersRoute);
app.use('/api/drivers', driversRoute);
app.use('/api/admin', adminRoute);
app.use('/api/emails', emailRoute);
app.use('/api/messages', twillio);
// app.use('/api/payments', paypal);
app.use('/api/payments', paymentRoute);

//This has to be right above port
app.use(apiErrorHandler);
//listen on port 5000
app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});