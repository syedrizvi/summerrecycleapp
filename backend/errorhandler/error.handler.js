// We don't want to crash our app and expose our stack trays each time an error happens, so We'll handle errors in this folder

// note: you have to pass in errors in the next(errorObj), for the error to be processed with this handler./

// we could also handle this on the frontend by sending it as res.json({error: error});
//express guide: https://expressjs.com/en/guide/error-handling.html

const ApiError = require('./ApiError');
const nonApiError = require('./nonApiError');

function apiErrorHandler(err, req, res, next){
    //in production avoid console.log or console.err because
    //it is not async
    // console.err(err);

    if(err instanceof ApiError){
        res.status(err.code).json(err.message);
        return;
    }

    if(err instanceof nonApiError){
        res.status(500).json(err.msg);
    }

    res.status(500).json("Something went wrong");
}

module.exports = apiErrorHandler;