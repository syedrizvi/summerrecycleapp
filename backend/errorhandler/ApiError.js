class ApiError{
    constructor(code, message) {
        this.code = code;
        this.message = message;
    }

    static badRequest(msg){
        return new ApiError(400, msg);
    }

    static badRequest401(msg){
        return new ApiError(401, msg);
    }

    static badCodeAndRequest(code, msg){
        return new ApiError(code, msg);
    }

    static internal(msg) {
        return new ApiError(500, msg);
    }
}

module.exports = ApiError;