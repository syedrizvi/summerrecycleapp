import jwtDecode from 'jwt-decode';
import React, { useState } from 'react';
import { Route, Redirect } from 'react-router-dom';
import auth from './authorization';
import useAuth from '../hooks/useAuth';
import useFindUser from '../hooks/useFindUser';
import axios from 'axios';

export const ProtectedRoutes = ({component: Component, ...rest}) => {
    const {logoutUser, error} = useAuth();

    return(
        //...rest gets whatever is passed in like path
        <Route {...rest} render={(props) => {
            let currentDate = new Date();
            // if()
            if(sessionStorage.getItem('accessToken') !== null)
            {
                let verification = jwtDecode(sessionStorage.getItem('accessToken'));
                if(verification.exp *1000 > Date.now()){//valid token
                // if(verification.exp *1000 < currentDate.getTime()){
                    return <Component />;//Uncomment When seperate routes for admin, driver, and customer are there.
                }
                else {//check if refresh token is invalid and refresh if it is
                    const refToken = sessionStorage.getItem('refreshToken');
                    const decodeRToken = jwtDecode(refToken);
                    if(decodeRToken.exp *1000 > Date.now()){ //valid
                        return <Component />;//Uncomment When seperate routes for admin, driver, and customer are there.
                    } else {
                        sessionStorage.removeItem('accessToken');
                        sessionStorage.removeItem('refreshToken');
                        console.log("user timed out!!");
                        logoutUser();
                        console.log("Came here");
                        return (
                            <Redirect to={{ pathname: '/signin', state: {from: props.location}}}/>
                        );
                    }
                }
            }
            else {
                logoutUser();
                return (
                    <Redirect to={{ pathname: '/signin', state: {from: props.location}}}/>
                );
            }
        }}
        />
    );
};

export default ProtectedRoutes;