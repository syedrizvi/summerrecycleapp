import jwtDecode from 'jwt-decode';
import axios from "axios";
import { useState} from 'react';
// import validation from "./validateToken"

// let validateToken = validation;
class Auth {
    constructor(){
        this.authenticated = false;
        this.user = null;
    }
    login(cb){
        this.authenticated = true;
        cb();
    }
    logout(cb) {
        this.authenticated = false;
        cb();
    }

    refreshToken(){
      const host = "https://recyclones.herokuapp.com/api";
        try{
          const res = axios.post( `${host}/refresh`, {token: sessionStorage.getItem('refreshToken') });
        //   setUser({
        //     ...user,
        //     accessToken: res.data.accessToken,
        //     refreshToken: res.data.refreshToken,
        //   });
          return (res.data);
        } catch (err){
          console.log(err);
        }
      };

    isAuthenticated(){
        // if(sessionStorage.getItem('accessToken') !== null)
        // {
        //     let currentDate = new Date();
        //     let verification = jwtDecode(sessionStorage.getItem('accessToken'));
        //     if(verification.exp * 1000 < currentDate.getTime()){
        //         const data = this.refreshToken();
        //         validateToken.refresh(this.refreshToken);
        //         data.header["authorization"] = "Bearer " + data.accessToken;
        //     }
        // }
        return this.authenticated;
    }
}

export default new Auth();

// const refreshToken = async() => {
//   try{
//     const res = await axios.post( "/refresh", {token: user.refreshToken });
//     setUser({
//       ...user,
//       accessToken: res.data.accessToken,
//       refreshToken: res.data.refreshToken,
//     });
//     return (res.data);
//   } catch (err){
//     console.log(err);
//   }
// };

// const axiosJWT = axios.create()

// axiosJWT.interceptors.request.use(
//   async(config) => {
//     let currentDate = new Date();
//     const decodeToken = jwt_decode(user.accessToken);
//     if(decodeToken.exp * 1000 < currentDate.getTime()){
//       const data = await refreshToken();
//       config.header["authorization"] = "Bearer " + data.accessToken;
//     }
//     return config;
//   },
//   (error) => {
//     return Promise.reject(error);
//   }
// );

// const handleSubmit = async (e) => {
//   e.preventDefault();
//   alert("HAndle submit call function");
//   try {
//     const res = await axios.post("/login", { username, password });
//     setUser(res.data);
//   } catch(err){
//     console.log(err);
//   }
// }

// const handleDelete = async (id) => {
//   setSuccess(false);
//   setError(false);
//   try{
//     await axiosJWT.delete("users/" + id, {
//       headers: { authorization: "Bearer " + user.accessToken },
//     });
//     setSuccess(true);
//   } catch(err) {
//     setError(true);
//   }
// };