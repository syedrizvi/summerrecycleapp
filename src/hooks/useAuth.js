import axios from "axios";
import { useState } from 'react';
import { Router, useHistory } from 'react-router-dom';
// import { useContext } from 'react';
// import { UserContext } from '../authorization/userContext';//to use in a page: const { user, setUser } = useContext(UserContext);
import jwtDecode from "jwt-decode";

export default function useAuth() {
    const host = "https://recyclones.herokuapp.com/api";
    const [email, setEmail] = useState(null);
    const [error, setError] = useState(null);
    // const { user, setUser } = useContext(UserContext);
    let history = useHistory();

    // add headers like this
    // axios.post('url', {email}, {
    // headers: {
    //     'authorization': 'Bearer ' + token
    //     }
    //   })
    //   .then((response) => {

    //   })
    //   .catch((err =>{ console.log(err);})
    // );
    //or 
    // axios({
    //     method: 'post', //you can set what request you want to be
    //     url: 'https://example.com/request',
    //     data: {id: varID},
    //     headers: {
    //       Authorization: 'Bearer ' + varToken
    //     }
    //   })

    const loginUser = async ({email, password}) => {
        setError(false);
        setEmail(email);
        // return await axios.post("/login", {email, password})
        return await axios.post(`${host}/login`, {email, password})
        .then((response) => {
            if(response.data.message){
                error =response.data.message; 
                setError(error);
            }else {
                  let token = response.data.accessToken;
                  sessionStorage.setItem("accessToken", response.data.accessToken);
                  sessionStorage.setItem("refreshToken", response.data.refreshToken);
                //   user = { id: response.data.user.id, email: response.data.user.email};
                  let verification = jwtDecode(response.data.accessToken);
                  let userrole = verification.userrole;
                  setError(null);
                //   setEr(user);
                if(userrole === 'driver'){
                    history.push("/driver");      
                }else if(userrole === 'admin'){
                    history.push("/user/admin");
                } else{
                    history.push("/user");
                }
            }
        }).catch((err) => {
            setError("Incorrect Email or Password!");
        });
    };

    const logoutUser = async() => {
        if(sessionStorage.getItem("accessToken") !== null){
            const refreshToken = sessionStorage.getItem("refreshToken");
            sessionStorage.removeItem("refreshToken");
            sessionStorage.removeItem("accessToken");
            history.push('/signin');
            // return await axios.post('/logout', (refreshToken))
            return await axios.post(`${host}/logout`, (refreshToken))
            .then((response) => {
                if(response.data.message){
                    error = response.data.message;
                    setError(error);
                }else {
                    // console.log("user logged out");
                }
            })
            .catch((err) => {
                // console.log(err);
                setError("User couldn't logout");
            })
        }else{
            history.push('/signin');
        }
    }

    return{
        loginUser,
        logoutUser,
        email,
        error
    }
}