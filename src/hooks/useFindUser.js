import {useState} from 'react';
import axios from "axios";
import jwtDecode from 'jwt-decode';
import useAuth from './useAuth';


export default function useFindUser(){
  const host = "https://recyclones.herokuapp.com/api";
  const [user, setUser] = useState(null);
  const [userError, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const {logoutUser, error} = useAuth();


  const axiosJWT = axios.create();//this has to be called to intercept the request. Instead of doing axios, you do axiosJWT.
  axiosJWT.interceptors.request.use(
    async(config) => {
      let token = sessionStorage.getItem('accessToken');
      // console.log(token);
      if( token !== null){
        let currentDate = new Date();
        const decodeToken = jwtDecode(token);
        if(decodeToken.exp *1000 < currentDate.getTime()){//expired
          const refToken = sessionStorage.getItem('refreshToken');
          const data = await refreshToken(refToken);            
          // console.log(data.accessToken);
          config.headers.authorization = `Bearer ${data.accessToken}`;
        }
        else{
          config.headers.authorization = `Bearer ${token}`;
        }
        return config;
      }else {
        logoutUser();
      }
    },
    (error) => {
      console.log(Promise.reject(error));
    }
  );

  const refreshToken = async(token) => {
    return await axios.post(`${host}/refresh`, {token: token})
      .then((response) => {
          // console.log(response.data.accessToken);
          if(response.data.message){
            // console.log(response.data.message);//error
              // error =response.data.message; 
              // setError(error);
              // setError(true);
          }else {
                sessionStorage.setItem("accessToken", response.data.accessToken);
                sessionStorage.setItem("refreshToken", response.data.refreshToken);
                return(response.data);
                // user = { id: response.data.user.id, email: response.data.user.email};
              //   setEr(user);
          }
      }).catch((err) => {
          setError(err.message);
      });            
  };

  const isLoggedIn = () => {
    let token = sessionStorage.getItem("accessToken");
    if(token !== null){
      //check if token expired and refresh the token if refresh token didn't expire
      return true;
    } else {
      return false;
    }
  };

  

  const getUsers = () => {
    let token = sessionStorage.getItem("accessToken");
    if(token !== null){
      let verification = jwtDecode(token);
      setUser({
        id: verification.id,
        email: verification.email,
        userrole: verification.userrole,
        exp: verification.exp
      });
    }
    else {
      setUser(null);
    }
    return (user);
  };
  
  const handleDelete = async (id) => {
    setSuccess(false);
    setError(false);
    try{
      await axiosJWT.delete(`${host}/users/` + id, {
        headers: { authorization: "Bearer " + user.accessToken },
      });
      setSuccess(true);
    } catch(err) {
      setError(true);
    }
  };

  return{
      isLoggedIn,
      handleDelete,
      refreshToken,
      getUsers,
      axiosJWT,
      error: userError
  }

}
