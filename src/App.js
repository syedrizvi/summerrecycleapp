import React from 'react';
import './App.css';
import axios from "axios";
import { useState} from 'react';
import jwt_decode from "jwt-decode";
import Admin from "./Pages/Admin/Admin";
import AdminModify from './Pages/Admin/AdminModify';
import Home from './Pages/Home';
import SignUp from './Pages/SignUp/SignUp';
import Footer from './Components/Footer/Footer';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import NavBar from "./Components/NavBar/NavBar";
import SignIn from './Pages/SignIn/SignIn';
import Payment from './Pages/Payment/Payment';
import SecurityCode from './Pages/SecurityCode/SecurityCode';
import User from './Pages/User/User'
import About from './Pages/About/About'
import Location from './Pages/Location/Location';
import Driver from './Pages/Driver/Driver';
import ProtectedRoutes from './authorization/protected.route';
import ProtectedAdminRoutes from './authorization/protectedAdmin.route';
import LocalStroageService from './authorization/localStorageService.js';
//import ErrorBoundary from './Components/ErrorBoundary';
import useAuth from './hooks/useAuth';
// import { UserContext } from './authorization/userContext';//to use in a page: const { user, setUser } = useContext(UserContext);
// import { useMemo } from 'react';//for useContext hook

function App() {
  const host = "https://recyclones.herokuapp.com/api";
  const [auth, setAuth] = useState(true);
  // const [user, setUser] = useState(null);//useContext var
  const localStorageService = LocalStroageService.getService();
  const {logoutUser, error} = useAuth();
  // const value = useMemo(() => ({user, setUser}), [user, setUser]);//useContext var
  
  const axiosJWT = axios.create()
  
  axiosJWT.interceptors.request.use(
    async(config) => {
      // console.log("Came To interceptors");
      let currentDate = new Date();
      if(sessionStorage.getItem('accessToken') !== null){
        const decodeToken = jwt_decode(sessionStorage.getItem('accessToken'));
        if(decodeToken.exp * 1000 < currentDate.getTime()){
          const data = await refreshToken();
          config.headers.authorization = `Bearer ${data.accessToken}`;
        }
        return config;
      }else {
        logoutUser();
      }
    },
    (error) => {
      console.log(Promise.reject(error));
    }
  );

  
  const refreshToken = async() => {
    const refreshToken = localStorageService.getRefreshToken();
    
    try{
      const res = await axios.post( `${host}/refresh`, {token: refreshToken });
      localStorageService.clearToken();
      // let tokenObj = { 
      //   accessToken: res.data.accessToken,
      //   refreshToken: res.data.refreshToken
      // }
      // localStorageService.setToken(tokenObj);
      sessionStorage.setItem("accessToken", res.data.accessToken);
      sessionStorage.setItem("refreshToken", res.data.refreshToken);
      return (res.data);
    } catch (err){
      console.log(err);
    }
  };


  return (
    <Router>
      <div className="App">
        {/* <ErrorBoundary> */}
        {/* </ErrorBoundary> */}
          <NavBar/>
            <Switch>
              {/* <UserContext.Provider value={value}> */}
                {/* <ErrorBoundary> */}
                  <Route path="/signIn" exact component={SignIn} />
                {/* </ErrorBoundary> */}
                {/* <ErrorBoundary> */}
                  <Route path="/signUp" exact component={SignUp} />
                {/* </ErrorBoundary> */}
                {/* <ErrorBoundary> */}
                  <Route path="/payment" exact component={Payment} />
                {/* </ErrorBoundary> */}
                {/* <ErrorBoundary> */}
                  <Route path="/securityCode" exact component={SecurityCode} />
                {/* </ErrorBoundary> */}
                {/* <ErrorBoundary> */}
                <Route path="/user" exact component={User} />
                {/* <ProtectedRoutes path="/user" exact component={User} /> */}
                {/* </ErrorBoundary> */}
                {/* <ErrorBoundary> */}
                  <Route path="/about" exact component={About} />
                {/* </ErrorBoundary> */}
                {/* <ErrorBoundary> */}
                  <Route path="/location" exact component={Location} />
                {/* </ErrorBoundary> */}
                {/* <ErrorBoundary> */}
                  {/* <Route path="/driver" exact component={Driver} /> */}
                {/* </ErrorBoundary> */}
                <ProtectedRoutes path="/driver" exact component={Driver} />
                {/* <ErrorBoundary> */}
                  <ProtectedRoutes path="/" exact component={Home}/>
                {/* </ErrorBoundary> */}
                <ProtectedAdminRoutes path="/user/admin" exact component={Admin} />
                <ProtectedAdminRoutes path="/user/admin/:userId" exact component={AdminModify} />
                {/* <ErrorBoundary> */}
                  {/* <Route path="/*" exact component={() => "404 not found"} /> */}
                {/* </ErrorBoundary> */}
                {/* <Route exact path="/"><Home/></Route> */}
                {/* <Route path="/home" exact component={Home} /> */}
              {/* </UserContext.Provider> */}
            </Switch>
          <Footer />
      </div>
    </Router>
  );
}

export default App;