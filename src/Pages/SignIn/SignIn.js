// import { useContext } from 'react';
// import {UserContext } from "../../authorization/userContext";
import img1 from '../../Components/images/Recycle.jpg';
import './SignIn.css';
import axios from "axios";
import { useState, useEffect } from 'react';
import { useHistory, Link, withRouter } from 'react-router-dom';
import useAuth from "../../hooks/useAuth";
import ReactGA from "react-ga";

ReactGA.initialize(process.env.GA_TRACKING_CODE);

const SignIn = () => {

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    });
    // let {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [loginStatus, setLoginStatus] = useState(null);
    const {loginUser, error} = useAuth();

    // const handleSubmit = async (e) => {
    //     e.preventDefault();
    //     try{
    //       await axios.post("/login", {email, password})
    //       .then((response) => {
    //           if(response.data.message){
    //             setLoginStatus(response.data.message);
    //         }else {
    //             console.log(response)
    //                 sessionStorage.setItem("accessToken", response.data.accessToken);
    //                 // user = { id: response.data.user.id, email: response.data.user.email};
    //                 // setUser(user);
    //                 history.push("/user");
                    
    //             }
    //         })         
    //     } catch (err) {
    //         setLoginStatus(err.message);
    //     }
    //   };
    const handleSubmit = async (e) => {
        e.preventDefault();
        await loginUser({email, password});
        if( error !== null){
            setLoginStatus(error);
        }
    }

    return (
        <div className="signIn">
            <div>     
             <img className="signIn--img" src={img1} alt="recycle" />
            </div>
            <div className="signIn--mid">
                <div className="signIn--welcomeH1">
                    <h1>Welcome to Recyclones</h1>
                </div>
                <form className="signIn--formBox"
                    // onSubmit={props.handleSubmit}
                    // onSubmit={props.loginHandler}
                    onSubmit={handleSubmit}
                >
                    <h4>Schedule your recycling pickup today!</h4>
                    
                    <label className="signIn--loginError">{loginStatus}</label>
                    <div className="signIn--formWrapper">
                        <label>Email</label>
                        <input 
                            type="text" 
                            name="email" 
                            placeholder="email"
                            onChange={(e) => setEmail(e.target.value)}
                             required/>
                        <label>Password</label>
                        <input type="password"
                        name="password"
                        placeholder="Password"
                        // value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                        />
                        <label className="signIn--forgotPassword">Forgot password</label>
                        <div className="signIn--row1Button">
                            <button className="signIn--signInButton signIn--button" type="submit">Sign In</button>
                            <button className="signIn--signUpnButton signIn--button" type="button"><Link to ="/signup">SignUp</Link></button>
                        </div>
                        <button className="signIn--continueButton signIn--button" type="button"><Link to ="/payment">Continue as guest</Link></button>
                    </div>
                    {/* <button type="submit">Sign Up</button> */}
                </form>
            </div>
        </div>
    )
}

export default withRouter(SignIn);
