import './AdminModify.css';
import {useState, useEffect} from 'react';
import Select from 'react-select';
import useFindUser from '../../hooks/useFindUser';
import { avatarGroupClasses } from '@mui/material';
import axios from 'axios';


const AdminModify = () => {
    const host = "https://recyclones.herokuapp.com/api";
    const {axiosJWT} = useFindUser();//
    const [driverId, setDriverId] = useState(null);
    const [loading, setLoading] = useState(false);
    const [avaialbeDriverId, setAvaiableDriverId] = useState([]);
    const [optionsDriverId, setOptionsDriverId] = useState([]);
    let orderId = window.location.href;
    const rex = /\w\W/i;
    orderId = orderId.split(rex);
    orderId = orderId[5];
    const handleConfirm = (e) => {
        e.preventDefault();

        console.log(driverId);
        console.log(orderId);
        
        axiosJWT.post(`${host}/admin/orders/assignDrivers`, {orderId, driverId})
        .then((response)=>{
            
        })
        .catch((err)=> {
            
        })
    }
    
    useEffect(()=>{
        getOptions();
    }, [])
    const getOptions = async()=>{
        setLoading(true);
        const res = await axiosJWT.get(`${host}/admin/drivers/available/id`)
        // console.log(res);
        const data = res.data;
    
        const options = data.map(d => ({
          "value" : d.user_id,
          "label" : d.user_id
        }))
        setOptionsDriverId({selectOptions: options})
        setLoading(false);
        // console.log(optionsDriverId);
      }

    if(loading){
        return(
            <h1>Loading...</h1>
        )
    }
  
    if(!loading){
        return (
            <div className="AdminModify--wrapper">
                <div className='AdminModify--userTitleContainer'>
                    <h1 className='AdminModify--userTitle'>Assign Driver Id to Order Id {orderId}</h1>
                    <Select  
                        value={driverId} 
                        options={optionsDriverId.selectOptions}
                        placeholder={driverId}
                        onChange={(e) => {setDriverId(e.value)}}
                    />
                    <button className='AdminModify--userAddButton' onClick={handleConfirm}>Assign</button>
                </div>
                
            </div>
        )
    }
}

export default AdminModify;
