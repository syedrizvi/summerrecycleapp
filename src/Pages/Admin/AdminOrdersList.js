import React from 'react';
import {useState} from 'react';
import {DataGrid} from '@material-ui/data-grid';
import './AdminOrdersList.css';
import {Link} from 'react-router-dom';
import useFindUser from '../../hooks/useFindUser';



const AdminOrdersList = ({orders, loading}) => {
    const {axiosJWT} = useFindUser();//
    const rows= [];
    const [driverid, setDriverid] = useState(null);
    const optionsDriverId = [
        {value: '1', label: '1'},
        {value: '2', label: '2'},
        {value: '3', label: '3'},
        {value: '4', label: '4'},
        {value: '5', label: '5'},
        {value: '5', label: '5'},
        {value: '6', label: '6'},
        {value: '7', label: '7'},
        {value: '8', label: '8'},
    ];

    const columns = [
        {field: 'id', headerName: 'Order id', width: 130},
        {field: 'user_id', headerName: 'User id', width: 130},
        {field: 'firstname', headerName: 'First', width: 105},
        {field: 'order_date', headerName: 'Date', width: 120},
        {field: 'order_time', headerName: 'Time', width: 110},
        {field: 'street_addr', headerName: 'Address', width: 170},
        {field: 'city', headerName: 'City', width: 110},
        {field: 'total', headerName: 'Total', width: 115},
        {field: 'order_status', headerName: 'Status', width: 120},
        {field: 'driver_id', headerName: 'Driver id', width: 150},
        {field: "action", headerName:"Action", width:120,
        renderCell:(params)=> {
            return(
                <Link to={'/user/admin/' + [params.row.id]}>
                    <button className='admin--ordersEditButton'>Edit</button>
                </Link>
            )
        }}
      ];

    const populateRow = async () => {
        for (let i = 0; i < orders.length; i++)
        {
            rows.push({
                id: orders[i].id,
                user_id: orders[i].user_id,
                driver_id: orders[i].driver_id,
                order_date: orders[i].order_date,
                order_time: orders[i].order_time,
                total: orders[i].total,
                order_status: orders[i].order_status,
                street_addr: orders[i].street_addr,
                city: orders[i].city,
                firstname: orders[i].firstname,
            })
        }
    }

    if(loading){
        return(
        <div>
            <h2>Loading....</h2>
        </div>)
    }
    if(!loading){
        populateRow();
        return(
        <div className="AdminOrdersList--mainWrapper">
            <div className="AdminOrdersList--heading">
                <h1>Unassigned Orders</h1>
            </div>
            <div style={{ height: 400, width: '100%' }}>
                <DataGrid
                    rows={rows}
                    columns={columns}
                    pageSize={5}
                    rowsPerPageOptions={[5]}
                    checkboxSelection
                />
            </div>
        </div>
        );
    }
        
}

export default AdminOrdersList
