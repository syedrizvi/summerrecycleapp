import React from 'react';
import { useState } from 'react';
import { withRouter } from 'react-router-dom';
import useFindUser from '../../hooks/useFindUser';
import useAuth from '../../hooks/useAuth';
// import { Admin, Resource} from 'react-admin';
import AdminOrdersList from './AdminOrdersList';
import './Admin.css';
import jwtDecode from 'jwt-decode';


const Admin = () => {
    const host = "https://recyclones.herokuapp.com/api";
    const {logoutUser, error} = useAuth();
    const [orderDataList, setOrderDataList] = useState([]);
    const {axiosJWT} = useFindUser();//boolean
    const [loading, setLoading] = useState(false);

    // Get Order Info once when component loads
  React.useEffect(() => {
    const  fetchData = async () => {
        setLoading(true);
        // Check if access token is in local storage
        if(sessionStorage.getItem("accessToken") !== null)
        {
            await axiosJWT.get(`${host}/admin/orders/unassigned`)
            .then(response => {
                // console.log(response.data)
                // Push data response of order info into array of JSONs
                for (let i = 0; i < response.data.length; i++) {
                    orderDataList.push({
                        id: response.data[i].order_id,
                        user_id: response.data[i].user_id,
                        driver_id: response.data[i].driver_id,
                        order_date: convertDate(response.data[i].order_date),
                        order_time: response.data[i].order_time,
                        total: response.data[i].total,
                        order_status: response.data[i].order_status,
                        street_addr: response.data[i].street_addr,
                        city: response.data[i].city,
                        firstname: response.data[i].firstname,
                    })
                  //   console.log(orderDataList);
                    // Push data response of box info into array using order id
                    // getBoxInfo(response.data[i].order_id);
                }
              //   console.log(orderDataList);
            })
            .catch(error => {
                console.log(error); 
            });
            setLoading(false);
        }else {
            // console.log("Driver logged out");
            logoutUser();
        }      
    }
    fetchData();
    }, []);

    // Convert database date to mm/dd/yy
    function convertDate(isoDate) {
        var date = new Date(isoDate);
        var year = date.getFullYear();
        var month = date.getMonth()+1;
        var dt = date.getDate();

        if (dt < 10) {
            dt = '0' + dt;
        }
        if (month < 10) {
            month = '0' + month;
        }

        return ' ' + month + '/'+ dt + '/' + year;
    }


    return (
        <div className="Admin--mainWrapper">
            <AdminOrdersList orders={orderDataList} loading={loading} />
        </div>
    )
}

export default withRouter(Admin);
