import React, { useState, useEffect } from 'react';
import img1 from '../../Components/images/Recycle.jpg'
import Calendar from 'react-calendar'
import 'react-calendar/dist/Calendar.css'
import './Payment.css'
import PayPal from '../../Components/PayPal';
import axios from "axios";
import{ withRouter } from 'react-router-dom';
import jwtDecode from 'jwt-decode';
import ReactGA from "react-ga";
import useFindUser from '../../hooks/useFindUser';
import Select from 'react-select';

ReactGA.initialize(process.env.GA_TRACKING_CODE);

const ColoredLine1 = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: 2
        }}
    />
);

const ColoredLine2 = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: 0.1
        }}
    />
);

var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });

const Payment = () => {

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    });
    const {axiosJWT} = useFindUser();

    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    const host = "https://recyclones.herokuapp.com/api";
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [zipCode, setZipCode] = useState("");
    const [email, setEmail] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [notes, setNotes] = useState("");
    const [orderType, setOrderType] = useState(0);
    const [date, setDate] = useState(tomorrow);
    const [time, setTime] = useState("");
    const [checkout, setCheckout] = useState(false);
    const [smallQty, setSmallQty] = useState(0);
    const [mediumQty, setMediumQty] = useState(0);
    const [largeQty, setLargeQty] = useState(0);
    const [subscriptionType, setSubscriptionType] = useState(null);
    const [subscriptionPeriod, setSubscriptionPeriod] = useState(null);
    const [subscriptionDescription, setSubscriptionDescription] = useState(null);

    const [orderInfo, setOrderInfo] = useState([]);
    const [tempPassword, setTempPassword] = useState("");
    const [newUserId, setNewUserId] = useState(null);

    const smallPrice = smallQty*4.00;
    const mediumPrice = mediumQty*6.00;
    const largePrice = largeQty*7.00;
    let total = smallPrice + mediumPrice + largePrice;

    const timeOptions = [
        { value: '8:00-9:00am', label: '8:00-9:00am' },
        { value: '9:00-10:00am', label: '9:00-10:00am' },
        { value: '10:00-11:00am', label: '10:00-11:00am' },
        { value: '11:00-12:00pm', label: '11:00-12:00pm' },
        { value: '12:00-1:00pm', label: '12:00-1:00pm' },
        { value: '1:00-2:00pm', label: '1:00-2:00pm' },
        { value: '2:00-3:00pm', label: '2:00-3:00pm' },
        { value: '3:00-4:00pm', label: '3:00-4:00pm' },
        { value: '4:00-5:00pm', label: '4:00-5:00pm' },
        { value: '5:00-6:00pm', label: '5:00-6:00pm' }
    ]

    // Create date ranges for Calendar
    const minDate = new Date();
    const maxDate = new Date();
    minDate.setDate(minDate.getDate() + 1);
    maxDate.setMonth(minDate.getMonth() + 3);

    // const changeOrderType = orderType => {
    //     resetOrderType();
    //     setOrderType(orderType);
    // }

    // const changeSubscriptionType = (e) => {
    //     setSubscriptionType(e);
    //     setSubscriptionTotal();
    // }
    
    // const changeSubscriptionPeriod = (e) => {
    //     setSubscriptionPeriod(e);
    //     setSubscriptionTotal();
    // }

    // function setSubscriptionTotal() {
    //     console.log("Type = ", subscriptionType);
    //     console.log("Period = ", subscriptionPeriod);
    //     if(subscriptionType == null || subscriptionPeriod == null) {
    //         console.log(total);
    //         setSubscriptionDescription("");
    //     }
    //     else if(subscriptionType == 1) 
    //     {
    //         if(subscriptionPeriod == 3)
    //         {
    //             total = 125;
    //             setSubscriptionDescription("3 Month Subscription (Weekly Pickup)");
    //         }
    //         else if(subscriptionPeriod == 6)
    //         {
    //             total = 200;
    //             setSubscriptionDescription("6 Month Subscription (Weekly Pickup)");
    //         }
    //         else if(subscriptionPeriod == 12)
    //         {
    //             total = 250;
    //             setSubscriptionDescription("12 Month Subscription (Weekly Pickup)");
    //         }
    //     }
    //     else if(subscriptionType == 2)
    //     {
    //         if(subscriptionPeriod == 3)
    //         {
    //             total = 50;
    //             setSubscriptionDescription("3 Month Subscription (Monthly Pickup)");
    //         }
    //         else if(subscriptionPeriod == 6)
    //         {
    //             total = 90;
    //             setSubscriptionDescription("6 Month Subscription (Monthly Pickup)");
    //         }
    //         else if(subscriptionPeriod == 12)
    //         {
    //             total = 120;
    //             setSubscriptionDescription("12 Month Subscription (Monthly Pickup)");
    //         }
    //     }
    // }

    // function resetOrderType()
    // {
    //     setSmallQty(0);
    //     setMediumQty(0);
    //     setLargeQty(0);
    //     setSubscriptionPeriod(null);
    //     setSubscriptionType(null);
    // }

    const getUsers = () => {
        let token = sessionStorage.getItem("accessToken");
        let user = [];
        if(token !== null){
          let verification = jwtDecode(token);
          user.push ({
            id: verification.id,
            email: verification.email,
            userrole: verification.userrole,
            exp: verification.exp
          });
        }
        else {
          user = null;
        }
        return (user);
    };
    // function validateEmail(email){//will return true or false
    //     const re = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    //     return re.test(String(email).toLowerCase());
    // }


    // Passes order data to PayPal.js

    const onSubmit = (e) => {
        e.preventDefault();

        let user = getUsers();
        let orderDate = date.toISOString().split('T')[0];

        if(user !== null)
        {
            orderInfo.push ({
                userId:user[0].id,
                orderDate:orderDate,
                orderTime:time,
                notes:notes,
                total:total,
                email: email.toLowerCase(),
                tempPassword: null,
                firstName: firstName
            });
            setCheckout(true);
        }
        else
        {
            // Create temp Password 
            // Create User
                // get user_id, email, tempPassword for orderInfo
                // push orderInfo
            const tempPassword = createPassword();
            createUser(email, tempPassword, tempPassword);
            // createEmailConfirmation(email);
            // createEmailTempPassword(email, tempPassword);
            
        }
        
    }

    const createUser = async (email, password, passwordRepeat) => {
        try{
            await axios.post(`${host}/registerUserInfo`, {email, password, passwordRepeat})
            .then((response) => {
                //   console.log(response);
                    if(response.data.message){
                        if(response.data.message ==="success!!" ){
                            console.log(`${email} is registered`);
                            //setNewUserId(response.data.id);
                            let orderDate = date.toISOString().split('T')[0];
                            orderInfo.push ({
                                userId: response.data.id,
                                orderDate:orderDate,
                                orderTime:time,
                                notes:notes,
                                total:total,
                                email: email,
                                tempPassword: password,
                                firstName: firstName
                            });
                            setCheckout(true); 
                                                     
                        }
                        else if(response.data.message ==="user already exists"){
                            let orderDate = date.toISOString().split('T')[0];
                            orderInfo.push ({
                                userId: response.data.id,
                                orderDate:orderDate,
                                orderTime:time,
                                notes:notes,
                                total:total,
                                email: email,
                                tempPassword: password,
                                firstName: firstName
                            });
                            setCheckout(true); 
                        }
                        else {
                            console.log(response.data.message);
                        }
                }
                if(response.data.error){
                    let error = response.data.error;
                    error = error.split("\"");
                    if( error.includes("passwordRepeat")){
                        error = "Passwords must match";
                    }
                    console.log(error);
                }
                else {
                    console.log(`${email} is registered`);
                    setNewUserId(response.data.id);
                }
            })         
            } catch (err) {
                console.log("Invalid Credentials " + err.message);
            // setSignUpStatus(err);
            }
        
    }

    function createPassword() {
        const crypto = require('crypto');
        const passwordBuf = crypto.randomBytes(6);
        const password = passwordBuf.toString('hex');

        return password;

    }

    return (
        <div className="payment">
            <img className="payment--img" src={img1} alt="recycle" />

                {checkout ? (
                    <div className="payment--paypalFormBoxContainer">
                        <div className="payment--paypalFormBox">
                            <button className="payment--button" type="submit" onClick={() => setCheckout(false)}>Go Back to Order</button>
                            <PayPal userValue={orderInfo} />
                        </div>
                    </div>
                ) : (
                    <form className="payment--mid"  onSubmit={onSubmit}>
                        <div className="payment--mid-2">
                            {/* Select Pickup Form */}
                            <div className="payment--formBox">
                                <h1>Select Pickup</h1>

                                <div className="payment--scheduleSplit">
                                    <div className="payment--columnCenter">
                                        <div style={{margin: '0px 0px 5px 0px'}}>
                                            <label style={{fontWeight: "bold", fontSize: "16px" }}>Selected Date: </label>
                                            {date.toDateString()}
                                        </div>

                                        <div style={{margin: '0px 0px 15px 0px', display: "flex", flexDirection: "row"}}>
                                            <label style={{fontWeight: "bold", fontSize: "16px" }}>Pickup Time: </label>
                                            <div style={{width: "180px", height: "20px", marginLeft: "10px", marginBottom: "10px"}}>
                                            <Select  value={time} options={timeOptions} placeholder={time} onChange={(e) => {setTime(e.value)}}/>
                                            </div>
                                        </div>
                                        <Calendar onChange={setDate} value={date} minDate={minDate} maxDate={maxDate}/>
                                    </div>

                                    <div className="payment--columnCenter" style={{ width: "200px", paddingTop: "50px" }}>
                                        {/* <label style={{fontWeight: "bold", fontSize: "16px" }}>
                                            Order Type
                                        </label>
                                        <div className="payment--radioOption">
                                            <label style={{ fontWeight: "500" }}>One-time Pickup</label>
                                            <input className="payment--radioInput"
                                            type="radio"
                                            value="0"
                                            name="orderType"
                                            onChange={(e) => changeOrderType(e.target.value)}
                                            defaultChecked
                                            />
                                        </div>
                                        <div className="payment--radioOption">
                                            <label style={{ fontWeight: "500" }}>Subscription&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            <input className="payment--radioInput"
                                            type="radio"
                                            value="1"
                                            name="orderType"
                                            onChange={(e) => changeOrderType(e.target.value)}
                                            />
                                        </div> */}

                                        {orderType == 0 ? (
                                        <div style={{padding: '80px 0px 0px 0px'}}>
                                            <label style={{ fontSize: "16px" }}>Box Size / Quantity</label>
                                            <div className="payment--boxInfo">
                                                <label style={{ fontWeight: "500" }}>Small ($4.00 each) : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                <select value={smallQty} onChange={(e) => setSmallQty(e.target.value)}>
                                                    <option value="0">0</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                </select>
                                            </div>
                                            <div className="payment--boxInfo">
                                                <label style={{ fontWeight: "500" }}>Medium ($6.00 each) : &nbsp;&nbsp;</label>
                                                <select value={mediumQty} onChange={(e) => setMediumQty(e.target.value)}>
                                                    <option value="0">0</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                </select>
                                            </div>
                                            <div className="payment--boxInfo">
                                                <label style={{ fontWeight: "500" }}>Large ($7.00 each) : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                <select value={largeQty} onChange={(e) => setLargeQty(e.target.value)}>
                                                    <option value="0">0</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                </select>
                                            </div>
                                        </div>
                                        ) : (
                                        // <div style={{padding: '80px 0px 0px 0px'}}>
                                        //     <label style={{ fontSize: "16px" }}>Select a Plan</label>
                                        //     <div>
                                        //         <select value={subscriptionType} onChange={(e) => {setSubscriptionType(e.target.value); setSubscriptionTotal()}}>
                                        //             <option value="">--------------------------</option>
                                        //             <option value="1">Pickup every Week</option>
                                        //             <option value="2">Pickup once a Month</option>
                                        //         </select>
                                        //     </div>
                                        //     <label style={{ fontSize: "16px" }}>Subscription Period</label>
                                        //     <div>
                                        //         <select value={subscriptionPeriod} onChange={(e) => {setSubscriptionPeriod(e.target.value); setSubscriptionTotal()}}>
                                        //             <option value="">----------</option>
                                        //             <option value="3">3 Months</option>
                                        //             <option value="6">6 Months</option>
                                        //             <option value="12">12 Months</option>
                                        //         </select>
                                        //     </div>
                                        //     <div>
                                        //         <select value={subscriptionType} onChange={(e) => {changeSubscriptionType(e.target.value || null)}}>
                                        //             <option value="">--------------------------</option>
                                        //             <option value="1">Pickup every Week</option>
                                        //             <option value="2">Pickup once a Month</option>
                                        //         </select>
                                        //     </div>
                                        //     <label style={{ fontSize: "16px" }}>Subscription Period</label>
                                        //     <div>
                                        //         <select value={subscriptionPeriod} onChange={(e) => {changeSubscriptionPeriod(e.target.value || null)}}>
                                        //             <option value="">----------</option>
                                        //             <option value="3">3 Months</option>
                                        //             <option value="6">6 Months</option>
                                        //             <option value="12">12 Months</option>
                                        //         </select>
                                        //     </div>
                                        // </div>
                                        null
                                        )}
                                        
                                    </div>
                                </div>
                            </div>

                            {/* Client Information Form */}

                            <div className="payment--formBox">
                                <h1>Client Information</h1>
                                <div className="payment--inputRow">
                                    <div className="payment--label-input">
                                        <label>First Name</label>
                                        <input
                                            type='text'
                                            name='firstName'
                                            value={firstName}
                                            placeholder='Enter First Name'
                                            size="30"
                                            onChange={(e) => setFirstName(e.target.value)}
                                            required
                                        />
                                    </div>

                                    <div className="payment--label-input" style = {{paddingLeft:"20px"}}>
                                        <label>Last Name</label>
                                        <input 
                                            type='text'
                                            name='lastName'
                                            value={lastName}
                                            placeholder='Enter Last Name'
                                            size="30"
                                            onChange={(e) => setLastName(e.target.value)}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="payment--inputRow">
                                    <div className="payment--label-input">
                                        <label>Street Address</label>
                                        <input
                                            type='text'
                                            name='address'
                                            value={address}
                                            placeholder='Enter Street Address'
                                            size="30"
                                            onChange={(e) => setAddress(e.target.value)}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="payment--inputRow">
                                    <div className="payment--label-input">
                                        <label>City</label>
                                        <input
                                            type='text'
                                            name='city'
                                            value={city}
                                            placeholder='Enter City'
                                            size="18"
                                            onChange={(e) => setCity(e.target.value)}
                                            required
                                        />
                                    </div>

                                    <div className="payment--label-input" style = {{margin:"0px 20px"}}>
                                        <label>State</label>
                                        <input 
                                            type='text'
                                            name='state'
                                            value={state}
                                            placeholder='CA'
                                            size="3"
                                            maxLength={2}
                                            onChange={(e) => setState(e.target.value)}
                                        />
                                    </div>

                                    <div className="payment--label-input">
                                        <label>Zip Code</label>
                                        <input 
                                            type='text'
                                            name='zip'
                                            value={zipCode}
                                            placeholder='Enter Zip'
                                            size="12"
                                            maxLength={5}
                                            onChange={(e) => setZipCode(e.target.value)}
                                        />
                                    </div>
                                </div>

                                <div className="payment--inputRow">
                                    <div className="payment--label-input">
                                        <label>Email</label>
                                        <input
                                            type='email'
                                            name='email'
                                            value={email}
                                            placeholder='Enter Email'
                                            size="30"
                                            onChange={(e) => setEmail(e.target.value)}
                                            required
                                        />
                                    </div>

                                    <div className="payment--label-input" style = {{paddingLeft:"18px"}}>
                                        <label>Phone Number (optional)</label>
                                        <input 
                                            style={{margin: '0px 0px 15px 0px'}}
                                            type='text'
                                            name='phoneNum'
                                            value={phoneNumber}
                                            placeholder='Enter Phone Number'
                                            size="30"
                                            maxLength={10}
                                            onChange={(e) => setPhoneNumber(e.target.value)}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className="payment--formBox">
                                <h1>Notes</h1>
                                <div>
                                    <textarea 
                                        style={{margin: '5px 30px 20px 30px', resize: 'none', fontFamily: 'Calibri', fontSize: '16px'}}
                                        value={notes}
                                        placeholder='Enter additional notes here'
                                        rows={5}
                                        cols={90}
                                        maxLength={300}
                                        onChange={(e) => setNotes(e.target.value)}
                                    />
                                </div>
                            </div>
                        </div>

                    <div className="payment--mid-2">

                        {/* Sticky SubTotal Form */}

                        <div className="payment--confirmBoxContainer">
                            <div className="payment--confirmFormBox">
                                <div className="payment--confirmLabels">
                                    <label>Item</label>
                                    <label>Item Cost</label>
                                </div>
                                <ColoredLine1 color="lightgray" />
                                {orderType == 0 ? (
                                <div>
                                    <div className="payment--confirmLabels">
                                        <label>Small Boxes x {smallQty}</label>
                                        <label>{formatter.format(smallPrice)}</label>
                                    </div>
                                    <div className="payment--confirmLabels">
                                        <label>Medium Boxes x {mediumQty}</label>
                                        <label>{formatter.format(mediumPrice)}</label>
                                    </div>
                                    <div className="payment--confirmLabels">
                                        <label>Large Boxes x {largeQty}</label>
                                        <label>{formatter.format(largePrice)}</label>
                                    </div>
                                </div>
                                ) : (
                                // <div className="payment--confirmLabels">
                                //     <label>{subscriptionDescription}</label>
                                //     <label>{formatter.format(total)}</label>
                                // </div>
                                null
                                )}
                                <ColoredLine2 color="lightgray" />

                                <div className="payment--confirmLabels2">
                                    <label style={{fontSize: '1.0rem'}}>Order Total</label>
                                    <label style={{fontSize: '1.0rem'}}>{formatter.format(total)}</label>
                                </div>
                                {total !== 0 && time !== null ? 
                                (<button className="payment--button" type="submit">Confirm and Pay</button>) :
                                (<button className="payment--buttonDisabled" disabled={true}>Confirm and Pay</button>)}
                            </div>
                        </div>
                    </div>

                </form>
                )}
            </div>
    );
}

//export default Payment;
export default withRouter(Payment);