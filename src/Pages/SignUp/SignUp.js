import img1 from '../../Components/images/Recycle.jpg'
import './SignUp.css'
import axios from "axios";
// import jwtDecode from 'jwt-decode';
import { useState, useEffect } from 'react';
import{ withRouter } from 'react-router-dom';
import ReactGA from "react-ga";

ReactGA.initialize(process.env.GA_TRACKING_CODE);

const SignUp = (props) => {

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    });


    const host = "https://recyclones.herokuapp.com/api";
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [passwordRepeat, setPasswordRepeat] = useState(null);
    const [signUpStatus, setSignUpStatus] = useState(null);

    const handleSubmit = async (e) => {
        e.preventDefault();
        try{
          await axios.post(`${host}/register`, {email, password, passwordRepeat})
          .then((response) => {
            //   console.log(response);
                if(response.data.message){
                    if(response.data.message ==="success!!" ){
                        setSignUpStatus(`${email} is registered`);
                    }else{
                        setSignUpStatus(response.data.message);
                    }
            }
            if(response.data.error){
                let error = response.data.error;
                error = error.split("\"");
                if( error.includes("passwordRepeat")){
                    error = "Passwords must match";
                }
                setSignUpStatus(error);
            }
            else {
                //   setSignUpStatus(`${email} is registered`);
              }
          })         
        } catch (err) {
            setSignUpStatus("Invalid Credentials");
        // setSignUpStatus(err);
        }
      };

    return (
        <div className="signUp">
            <img className="signUp--img" src={img1} alt="recycle" />
            <div className="signUp--mid">
                <div className="signUp--welcomeH1">
                    <h1>Welcome to Company Name</h1>
                </div>
                <form className="signUp--formBox"
                onSubmit={handleSubmit}
                >
                    <h4>Please enter your information</h4>

                    <label>{signUpStatus}</label>
                    <div className="signUp--formWrapper">

                        <label>Email Address</label>
                        <input 
                            type="text" 
                            name="email" 
                            placeholder="Enter Email Address" 
                            onChange={(e) => setEmail(e.target.value)}
                            required/>

                        <label>Password</label>
                        <input 
                            type="password"
                            name="password"
                            placeholder="Enter Password"
                            onChange={(e) => setPassword(e.target.value)}
                            required/>

                        <label>Confirm Password</label>
                        <input 
                            type="password"
                            name="password"
                            placeholder="Enter Password"
                            onChange={(e) => setPasswordRepeat(e.target.value)}

                            required/>

                        {/* <label>Street Address</label> 
                        <input type="text" name="street address" placeholder="Enter Street Address" required/>
                        <div className="signUp--rowInput">
                            <div className="signUp--state">
                                <label>State</label>
                                <input type="text" name="state" placeholder="Enter State" required/>
                            </div>
                            <div className="signUp--zipcode">
                                <label>Zip Code</label>
                                <input type="text" name="zip" placeholder="Enter Zip Code" required/>
                            </div>
                        </div> */}
                        
                        
                        
                        <div className="signUp--row1Button">
                            <button className="signUp--signUpButton signUp--button" type="submit">Sign Up</button>
                        </div>
                    </div>
                    {/* <button type="submit">Sign Up</button> */}
                </form>
            </div>
        </div>
    )
}

export default withRouter(SignUp);
