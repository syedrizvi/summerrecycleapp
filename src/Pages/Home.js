import React, {useEffect, useState} from 'react';
import {withRouter, Switch} from "react-router-dom";
import ReactGA from "react-ga";
import useFindUser from '../hooks/useFindUser';
import Admin from '../Pages/Admin/Admin';
import User from '../Pages/User/User';
import Driver from '../Pages/Driver/Driver';
import ProtectedRoutes from '../authorization/protected.route';
import ProtectedAdminRoutes from '../authorization/protectedAdmin.route';
import { useHistory } from 'react-router-dom';
import jwtDecode from 'jwt-decode';


ReactGA.initialize("UA-211579976-1");

// import { useContext } from 'react';
// import {UserContext} from '../authorization/userContext';
// rafce
// import img1 from './images/Recycle.jpg'
const Home = () => {
    const {getUsers} = useFindUser();
    let history = useHistory();
    // const {user} = useContext(UserContext);

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    });

    // let user = getUsers();
    // if(user != null){
    //     console.log(user)
    //     if(user.userrole === 'driver'){
    //         return(
    //             <Switch>
    //                 <ProtectedRoutes component={Driver}/>
    //             </Switch>
    
    //         )     
    //     }else if(user.userrole === 'admin'){
    //         return (
    //             <Switch>
    //                 <ProtectedAdminRoutes component={Admin}/>
    //             </Switch>
    //         )
    //     } else{
    //         return(
    //             <Switch>
    //                 <ProtectedRoutes component={User}/>
    //             </Switch>
    //         )
    //     }
    // }
    let token = sessionStorage.getItem('accessToken');
    if(token){
        let user = jwtDecode(token);
        if(user.userrole === 'driver'){
            return(
                <Switch>
                    <ProtectedRoutes component={Driver}/>
                </Switch>
    
            )     
        }else if(user.userrole === 'admin'){
            return (
                <Switch>
                    <ProtectedAdminRoutes component={Admin}/>
                </Switch>
            )
        } else{
            return(
                <Switch>
                    <ProtectedRoutes component={User}/>
                </Switch>
            )
        }
    }
}

export default withRouter(Home);
