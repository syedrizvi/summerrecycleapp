import React, {useEffect} from 'react';
import img1 from '../../Components/images/Recycle.jpg'
import './About.css'
import{ withRouter } from 'react-router-dom';
import ReactGA from "react-ga";

ReactGA.initialize(process.env.GA_TRACKING_CODE);


const About = () => {

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    });

    return (
        <div className="about">
            <img className="about--img" src={img1} alt="recycle" />
            <div className="about--mid">
                <form className="about--formBox"
                // onSubmit={this.handleSubmit}
                >
                    <h4>ReCyclones</h4>
                    <h6> Recyclones is a full-service recycling company that provides on-demand service picking up cardboards, aluminium, glass, oil and more. We always do our best to dispose of items in the greenest way possible. By providing our service in the Sacramento region, we’re able to enrich the economy of the communities we serve.</h6>
                </form>
            </div>
        </div>
    )
}

export default withRouter(About);
