import React, {useEffect} from 'react';
import img1 from '../../Components/images/Recycle.jpg'
import './SecurityCode.css'
import {withRouter} from 'react-router-dom';
import ReactGA from "react-ga";

ReactGA.initialize(process.env.GA_TRACKING_CODE);


const SecurityCode = () => {

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    });
    return (
        <div className="securityCode">
            <img className="securityCode--img" src={img1} alt="recycle" />
            <div className="securityCode--mid">
                <div className="securityCode--welcomeH1">
                    <h1>Please Enter Security Code</h1>
                </div>
                <form className="securityCode--formBox"
                // onSubmit={this.handleSubmit}
                >
                    <h4>Code will expire in 2 minutes</h4>
                    <div className="securityCode--formWrapper">
                        <label>Security Code</label>
                        <input type="text" name="securityCode" placeholder="Enter Security Code" required/>
                        <button className="securityCode--verificationButton securityCode--button" type="submit">Verify</button>
                    </div>
                    {/* <button type="submit">Verify</button> */}
                </form>
            </div>
        </div>
    )
}

export default withRouter(SecurityCode);
