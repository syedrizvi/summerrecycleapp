import React, {useEffect} from 'react';
import img2 from '../../Components/images/location.png'
import './Location.css'
import{ withRouter } from 'react-router-dom';
import ReactGA from "react-ga";

ReactGA.initialize(process.env.GA_TRACKING_CODE);


const Location = () => {

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    });
    return (
        <div className="location">
                <form className="location_formBox">

                <div className="intro">
                    <h1>Recyclone is a service localized in Sacramento, California</h1>
                    <p>Be sure you are a resident before applying!</p>
                </div>

                <div className="image">
                    <img className="location_img" src={img2} alt="sacramento" />
                </div>
                    
                </form>
        </div>
    )
}

export default withRouter(Location);