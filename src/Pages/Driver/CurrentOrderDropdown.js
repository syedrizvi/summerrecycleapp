import React from 'react';
import { withRouter } from 'react-router';
import { useState } from 'react';
import { FiChevronDown } from "react-icons/fi";
import { FiChevronUp } from "react-icons/fi";
import IconButton from '@mui/material/IconButton';
import CurrentOrdersItem from './CurrentOrdersItem';
import "./CurrentOrders.css";

const CurrentOrderDropdown = (props) => {

    const [open, setOpen] = useState(false);

    function CurrentOrdersDropdownOpen() {
        return (
            <div className = "CurrentOrders--DropdownContainer">
                <header className = "CurrentOrders--DropdownTitle">
                    <div className = "CurrentOrders--fdTitleText">
                        {props.orderInfoItem.order_date}
                    </div> 
                    <IconButton onClick = {() => {setOpen(!open)}}>
                        <FiChevronUp></FiChevronUp>
                    </IconButton> 
                </header>
                <CurrentOrdersDropdownContent></CurrentOrdersDropdownContent>
            </div>
        )
    }

    function CurrentOrdersDropdownClosed() {
        return (
            <div className = "CurrentOrders--DropdownContainer">
                <header className = "CurrentOrders--DropdownTitle">
                    <div className = "CurrentOrders--fdTitleText">
                        {props.orderInfoItem.order_date}
                    </div> 
                    <IconButton onClick = {() => {setOpen(!open)}}>
                        <FiChevronDown></FiChevronDown>
                    </IconButton>
                </header>
            </div>
        )
    }

    function CurrentOrdersDropdownContent() {
        return ( <CurrentOrdersItem userInfo = {props.userInfo} orderInfoItem = {props.orderInfoItem} boxInfoItem = {props.boxInfoItem}></CurrentOrdersItem>);
        // Loop for returning React Components
        

        // let renderedItemArray = [];

        // if (props.orderInfoList.length > 0) {
        //     for (let i = 0; i < props.orderInfoList.length; i++) {
        //         renderedItemArray.push(<CurrentOrdersItem userInfo = {props.userInfo} orderInfoItem = {props.orderInfo} boxInfoItem = {props.boxInfoList[i]}></CurrentOrdersItem>);
        //     }
        //     return renderedItemArray;
        // }
        // else {
        //     return (<div>You have 0 previous orders...</div>)
        // }
        
        // <CurrentOrdersItem userInfo = {props.userInfo} orderInfoItem = {props.orderInfoItem} boxInfoItem = {props.boxInfoItem}></CurrentOrdersItem>;
    }

    if (open === true) {
        return <CurrentOrdersDropdownOpen/>;
    }
    else {
        return <CurrentOrdersDropdownClosed/>;
    }



}

export default CurrentOrderDropdown;