import React from 'react';
import {useState, useEffect} from 'react';
import axios from "axios";
import jwtDecode from 'jwt-decode';
import { useHistory, Link, withRouter } from 'react-router-dom';
import './Driver.css';
import FormDropdown from '../../Components/FormDropdown';
import useAuth from '../../hooks/useAuth';
import useFindUser from '../../hooks/useFindUser';
import ReactGA from "react-ga";

ReactGA.initialize(process.env.GA_TRACKING_CODE);

function Driver() {

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    });
  // User information state variables
  const host = "https://recyclones.herokuapp.com/api";
  const [userName, setUserName] = useState("unknown");
  const [userData, setUserData] = useState(null);
  const [orderDataList, setOrderDataList] = useState([]);
  const [boxDataList, setBoxDataList] = useState([]);
  let history = useHistory();
  const {logoutUser} = useAuth();
  const {axiosJWT} = useFindUser();//boolean

  // Get User Info once when component loads
  React.useEffect(() => {
    // Check if access token is in local storage
    if(sessionStorage.getItem("accessToken") !== null)
    {
        // Get user id from local storage
        let verification = jwtDecode(sessionStorage.getItem("accessToken"));
        let user_id = verification.id;

        // API call for user info
        axiosJWT.post(`${host}/users/getUserInfo`, {userId: user_id})
        .then(response => {
          // Set state of user info to response data
          setUserData(response.data);
          setUserName(response.data.firstname);
        }).catch(error => {
            console.log(error.message); 
        });
    }     
    else
    {
        logoutUser();
        console.log("Driver log out");
    }
  }, []);

  // Get Order Info once when component loads
  React.useEffect(() => {
      // Check if access token is in local storage
      if(sessionStorage.getItem("accessToken") !== null)
      {
          // Get user id from local storage
          let verification = jwtDecode(sessionStorage.getItem("accessToken"));
          let user_id = verification.id;

          // API call for order info
          axiosJWT.post(`${host}/drivers/getOrderInfo`, {driverId: user_id})
          .then(response => {
              // Push data response of order info into array of JSONs
              for (let i = 0; i < response.data.length; i++) {
                  orderDataList.push({
                      order_id: response.data[i].order_id,
                      user_id: response.data[i].user_id,
                      driver_id: response.data[i].driver_id,
                      order_date: convertDate(response.data[i].order_date),
                      order_time: response.data[i].order_time,
                      pick_up_placement_note: response.data[i].pick_up_placement_note,
                      order_comment: response.data[i].order_comment,
                      total: response.data[i].total,
                      order_status: response.data[i].order_status,
                  })
                //   console.log(orderDataList);
                  // Push data response of box info into array using order id
                  getBoxInfo(response.data[i].order_id);
                  console.log(response.data[i].order_id);
              }
            //   console.log(orderDataList);
          }).catch(error => {
              console.log(error.message); 
          });
      }     
      else
      {
          console.log("Driver log out");
          // history.push("/signin");
        //   logoutUser();
      }      
  }, []);

  // Push Box Info using order_id
  function getBoxInfo(order_id) {

      // API call for box info
      axiosJWT.post(`${host}/drivers/getBoxInfo`, {orderId: order_id})
      .then(response => {
          boxDataList.push({
              order_id: response.data.order_id,
              box_size: response.data.box_size,
              quantity: response.data.quantity,
          })
          //setBoxDataList(boxDataArray);
      }).catch(error => {
          console.log(error.message);
      });      
  }

  // Convert database date to mm/dd/yy
  function convertDate(isoDate) {
      var date = new Date(isoDate);
      var year = date.getFullYear();
      var month = date.getMonth()+1;
      var dt = date.getDate();

      if (dt < 10) {
          dt = '0' + dt;
      }
      if (month < 10) {
          month = '0' + month;
      }

      return ' ' + month + '/'+ dt + '/' + year;
  }

  // CSS
  return (
    <div className="body">
          <div className="body">
            <div className="formContainer">
                <div className= "formUser">
                  <div className="formUserText">
                      {userName}
                  </div>
                </div>
                 <FormDropdown title = {"Account Settings"} userInfo = {userData}></FormDropdown>
                 <FormDropdown title = {"Current Orders"} userInfo = {userData} orderInfoList = {orderDataList} boxInfoList = {boxDataList}></FormDropdown>
                 {/* <FormDropdown title = {"Current Orders"} userInfo = {userData} orderInfoList = {orderDataList} boxInfoList = {boxDataList}></FormDropdown> */}
                 {/* <FormDropdown title = {"Upcoming Pickups"} userInfo = {userData} ></FormDropdown> */}
            </div>
          </div>
    </div>
  );
}

export default withRouter(Driver);
