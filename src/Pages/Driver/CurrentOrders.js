import React, {useEffect} from 'react';
import { useHistory, Link, withRouter } from 'react-router-dom';
import '../../Components/FormDropdown.css';
import CurrentOrderDropdown from './CurrentOrderDropdown';
import ReactGA from "react-ga";

ReactGA.initialize(process.env.GA_TRACKING_CODE);


const CurrentOrders = (props) => {

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    });

    // Return array of Order History Items
    function OrderHistoryContent() {

        // Loop for returning React Components
        let renderedItemArray = [];

        if (props.orderInfoList.length > 0) {
            for (let i = 0; i < props.orderInfoList.length; i++) {
                if (props.orderInfoList[i].order_status !== "Delivered" || props.orderInfoList[i].order_status !== "Cancelled") 
                {
                    renderedItemArray.push(<CurrentOrderDropdown title = {props.title} userInfo = {props.userInfo} orderInfoItem = {props.orderInfoList[i]} boxInfoItem = {props.boxInfoList[i]}></CurrentOrderDropdown>);
                } 
            }
        }
        // Output CSS
        if (renderedItemArray.length > 0) return renderedItemArray; 
        else return (<div> You have 0 current orders... </div>);
        
    }

    // CSS
    return (
        <div>
            <OrderHistoryContent></OrderHistoryContent>
        </div>
    )

}



export default withRouter(CurrentOrders);