import React from 'react';
import {useState} from 'react';
import useFindUser from '../../hooks/useFindUser';
import Select from 'react-select';

const CurrentOrdersItem = (props) => {
    const host = "https://recyclones.herokuapp.com/api";
    const [orderUpdate, setOrderUpdate] = useState();
    const [orderStatus, setOrderStatus] = useState();
    const {getUsers, axiosJWT} = useFindUser();//boolean
    const numbers = 0;

    
    

    const handleSubmit = (e) => {
        // const order_id = mysql_real_escape_string (req.body.orderId);
    // const driver_id = mysql_real_escape_string (req.body.driverId
    // const order_status = mysql_real_escape_string (req.body.order_status);
        e.preventDefault();
        let user = getUsers();
        let currentStatus = props.orderInfoItem !== null ? props.orderInfoItem.order_status : "Not Loaded";
        if(orderStatus !== currentStatus){
            const orderId = props.orderInfoItem.order_id;
            // console.log(orderStatus);
            let order_status = orderStatus;
            axiosJWT.post(`${host}/drivers/orders/updatestatus`, {order_status, orderId})
            .then((response) => {
                // console.log(`From CurrentOrdersItem line 20 ${response}`);
                // console.log(response);
                //for some reason token expires if anuthing is done here;
            })
            .catch((err) =>{
                // console.log(`From CurrentOrdersItem line 23 catch block \n${err}`);
            });
        }
    }
    return(
        <div className= "orderDropdownItemContainer">
            <div className= "fdTitleBar">
                <div className= "fdAccountInfoItemText">Order Pickup</div>
                <div className=""></div>
                <div className= "fdAccountInfoItemText">{props.orderInfoItem !== null ? props.orderInfoItem.order_date : "Not Loaded"}</div>
            </div>
            <div className= "CurrentOrderItem--fdCurrentOrderInfoWrapper">
                <div className="CurrentOrderItem--OrderInformationWrapper">
                    <div className= "CurrentOrderItem--fdInfoItem">
                        <div className= "CurrentOrderItem--fdAInfoItemText">Order ID:</div>
                        <div className= "CurrentOrderItem--fdAInfoItemText">{props.orderInfoItem !== null ? props.orderInfoItem.order_id : "Not Loaded"}</div>
                    </div>
                    <div className= "CurrentOrderItem--fdInfoItem">
                        <div className= "CurrentOrderItem--fdAInfoItemText">Date:</div>
                        <div className= "CurrentOrderItem--fdAInfoItemText">{props.orderInfoItem !== null ? props.orderInfoItem.order_date : "Not Loaded"}</div>
                    </div>
                    <div className= "CurrentOrderItem--fdInfoItem">
                        <div className= "CurrentOrderItem--fdAInfoItemText">Time:</div>
                        <div className= "CurrentOrderItem--fdAInfoItemText">{props.orderInfoItem !== null ? props.orderInfoItem.order_time : "Not Loaded"}</div>
                    </div>
                    <div className= "CurrentOrderItem--fdInfoItem">
                        <div className= "CurrentOrderItem--fdAInfoItemText">Address:</div>
                        <div className= "CurrentOrderItem--fdAInfoItemText">{props.userInfo !== null ? props.userInfo.street_addr : "Not Loaded"}</div>
                    </div>
                    <div className= "CurrentOrderItem--fdInfoItem">
                        <div className= "CurrentOrderItem--fdAInfoItemText">Size:</div>
                        <div className= "CurrentOrderItem--fdAInfoItemText">{props.boxInfoItem !== null ? props.boxInfoItem.quantity : "Not Loaded"}</div>
                    </div>
                    <div className= "CurrentOrderItem--fdInfoItem">
                        <div className= "CurrentOrderItem--fdAInfoItemText">Pickup Notes:</div>
                        <div className= "CurrentOrderItem--fdAInfoItemText">{props.orderInfoItem !== null ? props.orderInfoItem.pick_up_placement_note : "Not Loaded"}</div>
                    </div>
                    <div className= "CurrentOrderItem--fdInfoItem">
                        <div className= "CurrentOrderItem--fdAInfoItemText">Comments:</div>
                        <div className= "CurrentOrderItem--fdAInfoItemText">{props.orderInfoItem !== null ? props.orderInfoItem.order_comment : "Not Loaded"}</div>
                    </div>
                    <div className= "CurrentOrderItem--fdInfoItem">
                        <div className= "CurrentOrderItem--fdAInfoItemText">Payment:</div>
                        <div className= "CurrentOrderItem--fdAInfoItemText">{props.orderInfoItem !== null ? props.orderInfoItem.total : "Not Loaded"}</div> 
                    </div>
                    <div className= "CurrentOrderItem--fdInfoItem">
                        <div className= "CurrentOrderItem--fdAInfoItemText">Status:</div>
                        <select onChange={(e) => setOrderStatus(e.target.value)} className = "CurrentOrderItem--fdAInfoItemText seelctStatement">
                            <option value={props.orderInfoItem !== null ? props.orderInfoItem.order_status : "Not Loaded"}>{props.orderInfoItem !== null ? props.orderInfoItem.order_status : "Not Loaded"}</option>
                            <option  value="in_progress">In Progress</option>
                            <option  value="couldnt_deliver">Couldnt Deliver</option>
                            <option  value="not_delivered">Not Delivered</option>
                            <option  value="delivered">Delivered</option>
                        </select>
                        {/* <div className= "CurrentOrderItem--fdAInfoItemText">{props.orderInfoItem !== null ? props.orderInfoItem.order_status : "Not Loaded"}</div>  */}
                    </div>
                </div>
                <div className="CurrentOrderItem--buttonWrapper">
                    <button className="CurrentOrderItem--confirmButton" onClick={handleSubmit}>Confirm</button>
                </div>
            </div>            
        </div>
    )
}

export default CurrentOrdersItem;