import React from 'react';
import {useMemo} from "react";
import {Link} from 'react-router-dom';
// import Recyclones from '../images/Recyclones.png';
import Recyclones from '../images/logo2.png';
import RecyclonesFull from '../images/Recyclones-logo.png';
import { useState } from 'react';
import './NavBar.css'
import {HiMenu} from 'react-icons/hi'
import useAuth from "../../hooks/useAuth";
import useFindUser from "../../hooks/useFindUser";

const NavBar = () => {
    const {isLoggedIn} = useFindUser();//returns boolean
    const [open, setOpen] = useState(false);
    let [user, setUser] = useState(isLoggedIn());
    const {logoutUser, error} = useAuth();
    // let user = isLoggedIn();

    // const updateLogout = () => {}
    // user = React.useMemo(() => isLoggedIn(), [user]);
    const updateLogout = React.useMemo(() => {
        return isLoggedIn();
    }, [user]);


    const navStyle = {
        color: 'white'
    };    

    return (
        <nav>
            <img className='Nav--Recyclones' src={Recyclones} alt="Recylones" width="52em" />
            <div className="Nav--middle">
                <div className="Nav--RecyclonesFull">
                    <img className='Nav--CompanyName' src={RecyclonesFull} alt="RecylonesFull" width="180em"/>  
                </div>
                {/*<div className="companyName"><h3>Recyclones</h3></div>*/}
                
                <ul className="nav-links">
                    <Link style={navStyle} to="/"><li>Home</li></Link>
                    <Link style={navStyle} to="/SignUp"><li>SignUp</li></Link>
                    <Link style={navStyle} to="/SignIn"><li>SignIn</li></Link>
                    <Link style={navStyle} to="/Payment"><li>Schedule</li></Link>
                    <Link style={navStyle} to="/About"><li>About</li></Link>
                    <Link style={navStyle} to="/Location"><li>Location</li></Link>
                </ul>
            </div>
            <div className="nav--topRightCorner">
                {!user &&                    
                    <div className="nav--whiteCorner">
                        <div className='signInUpWrapper'>
                            <div className="nav--SignIn__Link">SignIn</div>
                            <div className="nav--Signup__Link">SignUp</div>
                        </div>
                        <div className="nav--avatar">.</div>
                    </div>
                }
                <div className="nav--topRightCornerLogoutWrapper">
                    {user &&
                        <button className="nav--logoutButton nav--topRightCornerLogout" onClick={() => {
                            logoutUser();
                            setUser(false);
                        }
                        }>Logout</button>
                    }
                </div>
                <div className="nav--space">  </div>
                <div className="nav--hamburgerMenu">
                    <HiMenu className="nav--hamicon" onClick={() => setOpen(!open)}/>
                    {open && 
                        <div className="nav--linksWrapper">
                            <ul className="nav--linksHamburgerMenu">
                                <Link className="nav--Link" style={navStyle} to="/"><li>Home</li></Link>
                                <div className="nav--hamLines"></div>
                                <Link className="nav--Link" style={navStyle} to="/SignUp"><li>SignUp</li></Link>
                                <div className="nav--hamLines"></div>
                                <Link className="nav--Link" style={navStyle} to="/SignIn"><li>SignIn</li></Link>
                                <div className="nav--hamLines"></div>
                                <Link className="nav--Link" style={navStyle} to="/Payment"><li>Schedule</li></Link>
                                <div className="nav--hamLines"></div>
                                <Link className="nav--Link" style={navStyle} to="/About"><li>About</li></Link>
                                <div className="nav--hamLines"></div>
                                <Link className="nav--Link" style={navStyle} to="/Location"><li>Location</li></Link>
                                <div className="nav--logoutButton nav--logoutButtonWrapper">
                                    {user && open && 
                                        <button className="nav--dropdownLogout nav--logoutButton" onClick={() => {
                                            logoutUser();
                                            setUser(false);
                                        }
                                    }>Logout</button>
                                    }
                                </div>
                            </ul>
                        </div>
                    }
                </div>
            </div>
        </nav>
    )
}
export default NavBar