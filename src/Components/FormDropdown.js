import React from 'react';
import axios from "axios";
import {useState, useEffect} from 'react';
import './FormDropdown.css';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { FiChevronDown } from "react-icons/fi";
import { FiChevronUp } from "react-icons/fi";
import IconButton from '@mui/material/IconButton';
import AccountSettings from '../Pages/User/AccountSettings';
import OrderHistory from '../Pages/User/OrderHistory';
import UpcomingPickups from '../Pages/User/UpcomingPickups';
import CurrentOrders from "../Pages/Driver/CurrentOrders.js";

// Example data
const postURL = "/changePassword";
const dataColumn = ["Date", "Time", "Address", "Size", "Notes", "Status"];
const dataRow = ["10/28/21", "4:30pm", "123 John Doe Ln", "14 boxes", "Pick-up boxes near left garage door", "Scheduled"];
const pickupDate = new Date(2021, 10 -1, 28);


const FormDropdown = (props) => {
    
    const [open, setOpen] = useState(false);
    

    if (open === true) {
        return <FormDropdownOpen/>;
    }
    else {
        return <FormDropdownClosed/>;
    } 

    function FormDropdownOpen() {
    
        return (
            <div className = "fdContainer">
                <header className = "fdTitle">
                    <div className = "fdTitleText">
                        {props.title}
                    </div> 
                    <IconButton onClick = {() => {setOpen(!open)}}>
                        <FiChevronUp></FiChevronUp>
                    </IconButton> 
                </header>
                <FormDropdownContent></FormDropdownContent>
            </div>
        )
    }

    function FormDropdownClosed() {
    
        return (
            <div className = "fdContainer">
                <header className = "fdTitle">
                    <div className = "fdTitleText">
                        {props.title}
                    </div> 
                    <IconButton onClick = {() => {setOpen(!open)}}>
                        <FiChevronDown></FiChevronDown>
                    </IconButton> 
                </header>
            </div>
        )
    }

    function FormDropdownContent() {
        if (props.title === "Account Settings") return <AccountSettings userInfo = {props.userInfo}></AccountSettings>; 
        if (props.title === "Order History") return <OrderHistory title = {props.title} userInfo ={props.userInfo} orderInfoList = {props.orderInfoList} boxInfoList = {props.boxInfoList} apiResponse={props.apiResponse}></OrderHistory>;
        if (props.title === "Upcoming Pickups") return <UpcomingPickups title = {props.title} userInfo ={props.userInfo} orderInfoList = {props.orderInfoList} boxInfoList = {props.boxInfoList} apiResponse={props.apiResponse}></UpcomingPickups>;
        if (props.title === "Current Orders") return <CurrentOrders userInfo ={props.userInfo} orderInfoList = {props.orderInfoList} boxInfoList = {props.boxInfoList}></CurrentOrders>;
    }

}

 





export default FormDropdown


