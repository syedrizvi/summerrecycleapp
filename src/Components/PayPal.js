import React, { useState, useEffect, useRef } from 'react'
import axios from "axios";

export default function PayPal(props) {
    const host = "https://recyclones.herokuapp.com/api";
    const paypal = useRef();
    console.log(props);
    const orderInfo = props.userValue[0];
    const [paidFor, setPaidFor] = useState(false);

    const handleOnApprove = async () => {

        // console.log(props.userValue[0]);

        try{
            await axios.post(`${host}/payments/createOrderEntry`, {orderInfo})
            .then((response) => {
                if(response.data === "order placed")
                {
                    createEmailConfirmation(orderInfo.email, orderInfo.firstName);
                    if (orderInfo.tempPassword !== null) createEmailTempPassword(orderInfo.email, orderInfo.firstName, orderInfo.tempPassword);
                }
              console.log(response);
            })         
          } catch (err) {
              console.log(err);
          }
    }

    useEffect(() => {
        const total = props.userValue[0].total;

        window.paypal.Buttons({
            createOrder: (data, actions, err) => {
                return actions.order.create({
                    intent: "CAPTURE",
                    purchase_units: [
                        {
                            description: "One-time order",
                            amount: {
                                currency_code: "USD",
                                value: total,
                            },
                        },
                    ],
                });
            },
            onApprove: async (data, actions) => {
                const order = await actions.order.capture();
                handleOnApprove();
                setPaidFor(true);
                console.log(order);
            },
            onError: (err) => {
                console.log(err);
            },
        }).render(paypal.current);
    }, []);

    function createEmailConfirmation(email, firstName) {
        try {
            axios.post(`${host}/emails/testing`, {
                to: email, 
                cc: "", 
                bcc: "", 
                subject: "Order Confirmation", 
                message: "Hello " + firstName + ", your order has been placed.", 
                attachment: "", 
                smtpDetails: ""
            })
            .then(res => {
                console.log(res); 
                console.log(res.data);
                // Set response and response color
                if (res.data === "mail sent") 
                {
                    console.log("success, email confirmation sent");
                } 
                else if (res.data === "mail not sent") 
                {
                    console.log("email not sent");
                }
                else 
                {
                    console.log("Network error");
                }
            })  
        }
        catch(err) {
            console.log(err.message);
        }
    }

    function createEmailTempPassword(email, firstName, tempPassword) {
        try {
            axios.post(`${host}/emails/testing`, {
                to: email, 
                cc: "", 
                bcc: "", 
                subject: "Temporary Password", 
                message: "Hello " + firstName + ", here is your temporary password if you wish to sign in next time. Password: " + tempPassword, 
                attachment: "", 
                smtpDetails: ""
            })
            .then(res => {
                console.log(res); 
                console.log(res.data);
                // Set response and response color
                if (res.data === "mail sent") 
                {
                    console.log("success, email temp password sent");
                } 
                else if (res.data === "mail not sent") 
                {
                    console.log("email not sent");
                }
                else 
                {
                    console.log("Network error");
                }
            })  
        }
        catch(err) {
            console.log(err.message);
        }
    }
    
    if(paidFor) {
        return (
            <div>
                <h1>
                    Thank you!
                </h1>
                <text>
                    Your order has been received and a confirmation email has been sent to {props.userValue[0].email}
                </text>
            </div>
        )
    }

    return (
        <div>
            <div ref={paypal}></div>
        </div>
    );
}
