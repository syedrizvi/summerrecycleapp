import React from 'react';
import './Footer.css'
const Footer = () => {
    return (
        <footer>
            <div className="contact-info">
                <div className="info">Contact us </div>
                <div className="info">Email: company@email.com </div>
                <div className="info">Phone: xxx-xxx-xxxx </div>
            </div>
            <div className="copyright">
                Copyrights &copy; NullPointerException.
            </div>
        </footer>
    )
}

export default Footer
