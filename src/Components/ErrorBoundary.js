import React, { Component } from 'react'

export class ErrorBoundary extends Component {
    //This component will not catch any event handler errors. Use try catch for that.
    //This will catch errors in rendering, during lifecycle methods and in the constructor under whole tree below them.
    //The placement of the error boundary matters as it controls if the entire app
    //should have the fall-back ui or just the component causing the problem.

    constructor(props){
        super(props);

        this.state = {
            hasError: false
        }
    }

    static getDerivedStateFromError(error){
        return{
            hasError: true
        }
    }

    componentDidCatch(error, info){
        console.log(error);
        console.log(info);
        //Debt: if we have a logging service we would output the error to the log file instead of console logging errors.
    }

    render() {
        if(this.state.hasError){
            return <h1>Something went wrong</h1>;
        }
        return this.props.children;
    }
}

export default ErrorBoundary
